# The RASSP Challenge: Hack a peer-to-peer distributed computation

## The RADIO Privacy Concept

Each node of a network has a local database that holds private,
sensitive data. This can be part of a telehealth, smart home, or
similar system.

A third party, e.g. medical research, power grid management, etc.,
has legitimate reason to run statistics over the local databases.
The third party has no need for individual data points.

An adversary, e.g. insurance companies, competing power supply
companies, burglars, etc., has utility for individual data points
they should not have access to.


## The RASSP Protocol

The RASSP Protocol provides privacy-preserving peer-to-peer
distributed computation of statistics. The querying application can
get the sum of values it never sees individually. The members of the
network exchange and process derivative values that cannot be
transformed back to the original secret values.

Core conceptual infrastructure existed but was never worked into a
full, implementable communications protocol. We designed and
implemented protocol and stack for peer-to-peer communication,
Scala/Java library for node clients and R library that abstracts the
library calls into statistical functions (t-test, average, etc.)


## The RASSP challenge

A Capture the flag hacking competition to test the RASSP protocol and
our implementation.

For the challenge 100 RASSP nodes setup as Debian VMs on the cloud.
All source code executing at the VMs is public and can be found at
https://bitbucket.org/dataengineering/rassp. The RASSP nodes are
deployed as Docker containers. The Docker image used can be found at
https://github.com/gmouchakis/docker-rassp.

Competition participants are granted full control of one of the nodes
so they know the secret database of exactly one node. Aggregate
queries can be executed by any participant from their own client
machine.

Although they control the query and participate in the network,
participants should not be able to discover other nodes' secret
values. Monetary reward for discovering secret values and further
reward for patching the security gap.
