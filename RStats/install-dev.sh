#! /bin/bash

set -o pipefail
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
FWDIR=$DIR/..
SRC=$FWDIR/RStats

R CMD build $SRC


if [ -z "$VERSION" ]; then
  VERSION=`grep Version $SRC/DESCRIPTION | awk '{print $NF}'`
fi

echo $VERSION
R CMD javareconf
R CMD INSTALL  `pwd`/radioStatistics_$VERSION.tar.gz

