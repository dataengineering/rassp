# RASSP STATISTICS #

In order to preserve privacy, we can not pass the raw data in the statistics. Hence, we describe and represent the data we want to participate in our statistics functions through special structures, and pass these structures as arguments to the statistics.

## DATA DESCRIPTION STUCTURES ##

* **Parameters:** Structure to describe the data that are needed for the computation of the privacy preserving statistic
functions
    * <arg1> Dependent: list of the Dependent Value (DV) or list of the values that take place in the correlated/linear statistics
    * <arg2> Groups: list of <GroupStat>

* **GroupStat:** Structure of a group that indicates the characteristics of a sample that are needed for a statistic computation. It contains a list of triples, where a triple has the form of :
    _<variable> <operator> <value>_

## IMPLEMENTED SECURE STATISTICS ##

**1. mean(params):**

  * **_Summary:_** The mean value of the subjects described by _params_
  
  * **_Arguments:_** A _<Parameters>_ variable, which needs a DV and/or one sample group
  
  * **_Results:_** A _<Double>_ number that represents the mean result
  
  * **_Example 1:_** You may want the mean of a single variable like the 'value\_time\_before'
  
    * Firstly you should define the DV through the arg1 of the _<Parameters>_:
    
      ```
        > mParam <- Parameters( list("value_time_before") , NULL)
      ```
    
    * Then you can run the secure statistic:
    
      ```
        > mean(mParam)
      ```

  * **_Example 2:_** We want the mean of the variable 'value\_time\_before' only for the male population (eg. sex=M)
    
    * The variable should be specified into the DV list , while all the conditions should be declared through a group (arg2):
    
      ```
         > mGr <- GroupStat( list( c("sex","=","M")) )
      ```
      
      ```
        > mParam <- Parameters( list("value_time_before") , list(mGr) )
      ```
    
    * Run the secure statistic:
    
      ```
        > mean(mParam)
      ```


**2. var(params):**

  * **_Summary:_** The variance value of the subjects, described by _params_
  
  * **_Arguments:_** A _<Parameters>_ variable, which needs a DV and/or one sample group
  
  * **_Results:_** A _<Double>_ number that represents the variance result
  
  * **_Example 1:_** We want the variance of a single variable like the 'value\_time\_before'
  
    * Firstly we should define the DV through the arg1 of the _<Parameters>_:
    
      ```
        > vParam <- Parameters( list("value_time_before") , NULL)
      ```
    
    * Then we can run the secure statistic:
    
      ```
        > var(vParam)
      ```
    
  * **_Example 2:_** Also we want the variance of the variable 'value\_time\_before' only for the male population (eg. sex=M)
    
      * The variable should be specified into the DV list (arg1), while all the conditions should be declared through a group (arg2):
      
        ```
          > vGr <- GroupStat( list( c("sex","=","M")) )
        ```
        
        ```
          > vParam <- Parameters( list("value_time_before") , list(vGr) )
        ```
      
      * Then we can run the secure statistic:
      
        ```
          > var(stParam)
        ```


**3. stdev(params):**

  * **_Summary:_** The standard deviation value of the subjects, described by _params_

  * **_Arguments:_** A _<Parameters>_ variable, which needs a DV and/or one sample group

  * **_Results:_** A _<Double>_ number that represents the standard deviation result
  
  * **_Example 1:_** We want the st. deviation of a single variable like the 'value\_time\_before'
  
    * Firstly we should define the DV through the dependent list (arg1) of the _<Parameters>_:
    
      ```
        > stParam <- Parameters( list("value_time_before") , NULL )
      ```
    
    * Now we can run the statistic:
    
      ```
        > stdev(stParam)
      ```
    
  * **_Example 2:_** Also we want the st.deviation of the variable 'value\_time\_before' only for the male population (eg. sex=M)
  
    * The variable should be specified into the DV list , while all the conditions should be declared through a group (arg2):
    
      ```
        > stGr <- GroupStat( list( c("sex","=","M")) )
      ```
      
      ```
        > stParam <- Parameters( list("value_time_before") , list(stGr) )
      ```
    
    * We can run the statistic as follows:
    
      ```
        > stdev(stParam)
      ```

**4. lr(params):**

  * **_Summary:_** Simple Linear Regression statistic, described by _params_. The statistic fits a straight line: 
  _y = beta1 + beta0 \* x_
  
  * **_Arguments:_** The statistic is performed for exactly one dependent (y) and one independent (x) variable. Thus a _<Parameters>_ argument it is needed, with 2 variables in the DV list (arg1):
  
    * The first input in the DV list declares the independent variable (x)
    
    * The second input in the DV list specifies the dependent variable (y)

  * **_Results:_**
    * The fitted straight line
    
    * The plot of the straight line
    
    * Some basic characteristics of the statistic’s computation, such as xbar, ybar etc
    
    * Some analysis characteristics of the statistic’s computation, such as Sx, Sy, R2
    
  * **_Example:_** We want to apply a linear regression test between the 2 variables: value\_time\_before (→ x), value\_time\_after (→ y)
  
    * Firstly we should define them through the DV list of _<Parameters>_:
    
      ```
        > lParam <- Parameters( list("value_time_before", "value_time_after") , NULL)
      ```
    
    * We can execute the secure statistic:
    
      ```
        > lr(lParam)
      ```

**5. cor(params):**

  * **_Summary:_** Correlation Coefficient statistic, described by _params_
  
  * **_Arguments:_** The statistic needs exactly two variables, that are used to check whether they are linearly related or not. Thus a _<Parameters>_ argument it is needed, with 2 variables in the DV list (arg1)
  
  * **_Results:_** A _<Double>_ number between the range [-1.0, 1.0], which denotes the linearly relation of the 2 variables
  
  * **_Example:_** We want to apply a correlation test on the 2 variables: value\_time\_before, value\_time\_after
  
    * Firstly we should define the variables through the _<Parameters>_:
    
      ```
        > corParam <- Parameters( list("value_time_before", "value_time_after") , NULL )
      ```
    
    * We can execute the secure statistic:
    
      ```
        > cor(corParam)
      ```

**6. normality(params, conf.level):**

  * **_Summary:_** Checks whether a sample, that is described through _params_, follows a normal distribution (D’ Agostino-Pearson omnibus method)
  
  * **_Arguments:_**
  
    * _<Parameters>_ variable, which needs a DV and a single sample group
    
    * conf.level (default 0.95), which specifies the confidence level (the opposite of the significant level) for the statistic
    
  * **_Results:_** Returns useful information about the testing method, like Zg1, Zg2 , pvalue, as well as the final decision if the sample follows a Gaussian distribution
  
  * **_Example:_** We want to check whether the variable 'value\_time\_after' follows a normal distribution for the sample group where the people are female (sex=F)
  
    * Firstly we should define the _<Parameters>_ with the DV (arg1) and the IV (arg2):
    
      ```
        > nGr <- GroupStat( list( c("sex", "=", "F") ))
      ```
      
      ```
        > nParam <- Parameters( list("value_time_after") , list(nGr))
      ```
    
    * Then we run the following statistic by using the default confidence level:
    
      ```
        > normality(nParam)
      ```

**7. plotnorm(params):**

  * **_Summary:_** Plots the Gaussian distribution of a sample that is described by _params_
  
  * **_Arguments:_** A _<Parameters>_ variable which needs DV and a single sample group
  
  * **_Results:_**
  
    * The plot of the normal distribution
    
    * The mean and st.devation of the distribution
    
  * **_Example:_** We want to plot the Gaussian distribution of the variable 'value\_time\_after' for the sample group where the people are female (sex=F)
  
    * Firstly we should define the _<Parameters>_ with the DV (arg1) and the conditions of the group (arg2):
    
      ```
        > nGr <- GroupStat( list( c("sex", "=", "F") ))
      ```
      
      ```
        > nParam <- Parameters( list("value_time_after") , list(nGr))
      ```
    
    * We can run the statistic:
    
      ```
        > plotnorm(nParam)
      ```

**8. chisq.test(params):**

  * **_Summary:_**  The Chi-Square (Pearson's method) Test statistic, described by _params_
  
  * **_Arguments:_**  A _<Parameters>_ variable, which needs two sample groups, each one defining the categories of each attribute 
  
  * **_Results:_** Some analytics of the Pearson's chi-square, such as df, X2, pvalue
  
  * **_Example:_** We want to apply a chi-square test between the following two variables: sex (with categories _M_ and _F_) and age (with categories _old_, _young_)
    * Firstly we should define the _<Parameters>_ value, by specifying the two sample groups (arg2). Notice that there is no DV in this occasion.
    
      ```
        > chiGr1 <- GroupStat( list( c("sex", "=", "M") , c("sex", "=", "F") ))
      ```
      
      ```
        > chiGr2 <- GroupStat( list( c("age", "=", "old") , c("age", "=", "young") ))
      ```
      
      ```
        > chiParam <- Parameters( list() , list(chiGr1, chiGr2))
      ```
    
    * We can execute the statistic:
    
      ```
        > chisq.test(chiParam)
      ```

**9. ttest(params, alternative, mu, varEq, conf.level):**

  * **_Summary:_** The T-test statistic, described by _params_ and the statistic's attributes
  
  * **_Arguments:_**
    * A _<Parameters>_ variable which needs exactly two sample groups
    * alternative: A character string specifying the alternative hypothesis, must be one of "two.sided" (default), "greater" or "less". You can specify just the initial letter
    * mu: A double number indicating the difference in means and declares the Ho hypothesis
    * varEq: A logical variable indicating whether to treat the two variances as being equal. If TRUE then the Student T-test is used otherwise the Welch (or Satterthwaite) approximation to the degrees of freedom is used
    * conf.level: The confidence level of the interval. It is the opposite of significance level
    
  * **_Results:_** A set of resulted components after the T-test execution, like df, p-value, t-value etc
  
  * **_Example:_** We want to apply a t-test on the DV 'value\_time\_before' where the independent variable is sex(M, F)
    * First we should define the eligibility criteria of the T-test, through the _<Parameters>_. Except the DV, we should specify two sample groups  (arg2) \- one for each category of the independent variable:
    
      ```
        > tGr1 <- GroupStat( list( c("sex", "=", "M") ))
      ```
      
      ```
        >  tGr2 <- GroupStat( list( c("sex", "=", "F") ))
      ```
      
      ```
        > tParam <- Parameters( list("value_time_before") , list(tGr1, tGr2) )
      ```
    
    * We can execute the t-test statistic for the Welch method, while keeping the rest of the arguments with their default values
    
      ```
        > ttest(tParam, varEq = FALSE)
      ```
    
**10. anova(params, method):**

  * **_Summary:_** The ANOVA statistic described by _params_. Regarding the _method_ the following types can be computed:
  
    * One-Way Between ANOVA
    
    * One-Way Within / Repeated ANOVA
    
    * Mixed ANOVA

  * **_Arguments:_** 
    * A _<Parameters>_ variable which is set regarding the ANOVA method. The One-Way Between ANOVA as well as One-Way Repeated ANOVA needs a single DV and at least two sample groups. On the other hand, Mixed ANOVA needs a single DV and exactly two groups (the one describing the sample groups and the other the conditions).
    * method: The method of anova that will be applied. Ex. 'b' is for one-way between subjects, 'r' is for repeated measures and 'm' for mixed

  * **_Results:_**  A set of resulted components (summary table) after the ANOVA execution, such as df, ssq, ms, Fvalue, pvalue etc

  * **_Example 1 (B-P):_**  We want to apply an one-way anova between participants on the DV 'value\_time\_before' and on two groups regarding their gender (sex=M/F)
  
    * Firstly we should define the _<Parameters>_:
    
      ```
        > gr1 <- GroupStat( list( c("sex", "=", "M") ))
      ```
      
      ```
        > gr2 <- GroupStat( list( c("sex", "=", "F") ))
      ```
      
      ```
        > btwParam <- Parameters( list("value_time_before") , list(gr1, gr2) )
      ```
    
    * We can run the one-way BP:
    
      ```
        > anova(btwParam)
      ```
    
    or
    
      ```
        > anova(btwParam, "b")
      ```

  * **_Example 2 (W-P):_** We want to apply an one-way anova within participants on the DV 'value' and on two conditions regarding the time (time=before/after)
  
    * Firstly we should define the _<Parameters>_:
    
      ```
        > gr1 <- GroupStat( list( c("time", "=", "before") ))
      ```
      
      ```
        > gr2 <- GroupStat( list( c("time", "=", "after") ))
      ```
      
      ```
        > repParam <- Parameters( list("value") , list(gr1, gr2) )
      ```
 
    * We can run the one-way WP:
    
      ```
        > anova(repParam, "r")
      ```
 
  * **_Example 3 (Mixed):_** We want to apply a mixed anova on the DV 'value', on two groups regarding their gender (sex=M/F) and on two conditions regarding the time (time=before/after)
  
    * Firstly we should define the _<Parameters>_. Group 1 should contain the groups, while Group 2 the conditions:
    
      ```
        > gr1 <- GroupStat( list( c("sex", "=", "M"), c("sex", "=", "F") ))
      ```
      
      ```
        > gr2 <- GroupStat( list( c("time", "=", "before"), c("time", "=", "after") ))
      ```
      
      ```
        > mixParam <- Parameters( list("value") , list(gr1, gr2) )
      ```
 
    * We can run the Mixed Anova:
    
      ```
        > anova(mixParam, "m") 
      ```

All the secure statistics are tested with the use of the given dataset, resulting in accurate values.
