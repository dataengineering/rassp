#' T-test statistic with privacy preserving
#'
#' T-test statistic that preserves the privacy of the data sources. The statistic is performed for exactly two samples.
#'
#' @param params The <Parameters> that should be defined to compute the statistic. T-test statistic needs exactly 2 groups-samples.
#' @param alternative A character string specifying the alternative hypothesis, must be one of "two.sided" (default), "greater" or "less". You can specify just the initial letter.
#' @param mu A double number indicating the difference in means and declares the Ho hypothesis.
#' @param varEq A logical variable indicating whether to treat the two variances as being equal.
#' If TRUE then the Student T-test is used otherwise the Welch (or Satterthwaite) approximation to the degrees of freedom is used.
#' @param conf.level The confidence level of the interval. It is the opposite of significance level.
#' @return A set of resulted components after the T-test execution, like p-value, t-value etc
#' @examples
#' We want to apply a t-test on the dependend variable 'value_time_before' where the independent variable is sex(M, F).
#' Firstly we should define the <Parameters>:
#' tGr1 <- GroupStat( list( c("sex", "=", "M") ))
#' tGr2 <- GroupStat( list( c("sex", "=", "F") ))
#' tParam <- Parameters( list("value_time_before") , list(tGr1, tGr2))
#'
#' To run the Welch method for t-test, type:
#' ttest(tParam, varEq = FALSE)
#'
#' @export
ttest <- function(params, alternative='t', mu=0.0, varEq=FALSE, conf.level=0.95)
{
  if(missing(params)) {
    stop("The argument 'params' should be filled in.")
  }

  if(class(params)[2] != "Parameters") {
    stop("The argument 'params' should be an instance of 'Parameters'.")
  }


  if(typeof(mu) != "double" || typeof(varEq) != "logical" || typeof(conf.level) != "double" || conf.level >= 1.0) {
    stop("You should specify the suitable formats in the arguments.\n
         - <mu>: double\n - <varEq>: boolean\n - <conf.level>: double and below 1.0")
  }

  if(alternative == "two-sided" || alternative == "t") {
    alternative = "t"
  }
  else if(alternative == "greater" || alternative == "g") {
    alternative = "g"
  }
  else if(alternative == "less" || alternative == "l") {
    alternative = "l"
  }
  else {
    alternative = "t"
    warning("Wrong input on 'alternative' parameter. Default ('two-sided') is taken.")
  }

  input <- .setInputTtest(params, alternative, mu, varEq, conf.level)

  testObj <- .jnew("gr.demokritos.iit.radio.home.orchestration.Controller")
  result <- .jcall(testObj, "Ljava/util/HashMap;", "tTestParam", input)

  .convertTtestResult(result)

  }

#Set the inputs for the T-test
.setInputTtest <- function(params, alternative, mu, varEq, conf.level) {
  # set the map with the fundamental information to compute the T-test
  map <- .jnew("java.util.HashMap")

  .jrcall(map,"put", "alternative", as.character(alternative))
  .jrcall(map,"put", "mu", as.character(mu))
  .jrcall(map,"put", "varEq", as.character(varEq))
  .jrcall(map,"put", "conf.level", as.character(conf.level))

  input <- .buildQuery(params, map)

  return(input)

}

#Builds the select clause of the query, as it is specified by the user
#@return: List<String> that defines the select clause
.buildSelectTtest <- function(params) {
  cor <- getDependency(params)

  selectList <- .jnew("java.util.ArrayList")

  for(i in 1:length(cor)) {
    .jrcall(selectList, "add", as.character(cor[i]))
  }

  return(selectList)
}


#Takes all the basic information to convert the where clause of the query
#@return: List<WhereClause> that defines the where clause
.buildWhereTtest <- function(group, msg) {

  #where <- .jnew("gr.demokritos.iit.radio.home.entities.qfr.WhereClause")

  triples <- getTriples(group)

  for (triple in triples) {
    vect <- .jnew("java.util.Vector")

    for(val in triple) {
      .jrcall(vect, "add", val)
    }
    .jcall(msg, "V","addTriple", vect)

  }

  return(msg)
}


.convertTtestResult <- function(result) {
  keySet<-.jrcall(result,"keySet")
  an_iter<-.jrcall(keySet,"iterator")
  aList <- list()

  while(.jrcall(an_iter,"hasNext")){
    key <- .jrcall(an_iter,"next");

    aList[[key]] <- .jrcall(result,"get",key)
  }

  return(aList)
}
