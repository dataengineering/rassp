package gr.demokritos.iit.radio.home.dsl

/*
trait Interpretation {
  val IR : Expressions

  import IR._

}
*/

trait InterpExp extends Compile with Base with Expressions
{

  def interp[T:Manifest](e : Exp[T]) : T = {
    e match {
      case Const(c) => c
      case sym@Sym(_) => interpSym(sym.asInstanceOf[Sym[T]])
      case _ => throw new Exception("Don't know how to interpret " + e)
    }
  }

  def interpSym[T:Manifest](sym : Sym[T]) : T = Def.unapply(sym) match {
    case Some(d) => interp(d)
    case None => throw new Exception("Don't know how to interpret for : " + sym)
  }

  def interp[T:Manifest](e : Def[T]) : T = {
    throw new Exception("Don't know how to interpret for : " + e)
  }

}


trait InterpNumericExp extends NumericOpsExp with InterpExp
{

  override def interp[T:Manifest](d : Def[T]) : T = d match {
      case e@NumericPlus(x, y)   => e.aev.asInstanceOf[Numeric[T]].plus(interp(x), interp(y))
      case e@NumericMinus(x, y)  => e.aev.asInstanceOf[Numeric[T]].minus(interp(x), interp(y))
      case e@NumericTimes(x, y)  => e.aev.asInstanceOf[Numeric[T]].times(interp(x), interp(y))
      case e@NumericDivide(x, y) => e.aev.asInstanceOf[Fractional[T]].div(interp(x), interp(y))
      case _ => super.interp(d)
  }

}

trait InterpIfThenElseExp extends IfThenElseExp with InterpExp {

  override def interp[T:Manifest](e : Def[T]) : T = e match {
    case IfThenElse(cond, i, t) => {
      val c : Boolean = interp(cond)
      c match {
        case true => interp(i)
        case false => interp(t)
      }
    }
    case _ => super.interp(e)
  }

}


trait InterpBooleanExp extends BooleanOpsExp with InterpExp {

  override def interp[T:Manifest](e : Def[T]) : T = e match {
    case BooleanNegate(b)  => interp(b).unary_!.asInstanceOf[T]
    case BooleanAnd(b1,b2) => (interp(b1) && interp(b2)).asInstanceOf[T]
    case BooleanOr(b1,b2)  => (interp(b1) || interp(b2)).asInstanceOf[T]
    case _ => super.interp(e)
  }

}

/*
trait InterpFunctionsExp extends FunctionsExp with InterpExp {

  override def interp[T:Manifest](e : Def[T]) : T = e match {
    case Lambda(x, b) => interp(b)
  }

}
*/

trait InterpDBAggrOpsExp extends DBAggrOpsExp with InterpExp {

  override def interp[T:Manifest](e : Def[T]) : T = e match {
    case Sum(se) => ??? //computeService()
    case _ => super.interp(e)
  }
}


trait InterpDBOpsExp extends DBOpsExp with InterpExp {
  override def interp[T:Manifest](e : Def[T]) : T = e match {
    case Map(v : Exp[DB[Any]], f : Exp[Any => T]) => interp(f).apply(interp(v))
    // case Filter(v : Exp[T], f : Exp[T => Boolean]) =>
    case _ => super.interp(e)
  }
}


trait InterpOrderingExp extends OrderingOpsExp with InterpExp {

  override def interp[T:Manifest](e : Def[T]) : T = implicitly[Manifest[T]].toString() match {
    case "Boolean"  => interpBoolean(e.asInstanceOf[Def[Boolean]]).asInstanceOf[T]
    case _ => super.interp(e)
  }

  def interpBoolean[T:Manifest](d : Def[Boolean]) : Boolean = d match {
      case e@OrderingEquiv(x : Exp[T],y : Exp[T]) => e.aev.equiv(interp(x), interp(y))
      case e@OrderingGTEQ(x : Exp[T],y : Exp[T]) => e.aev.gteq(interp(x), interp(y))
      case e@OrderingGT(x : Exp[T],y : Exp[T]) => e.aev.gt(interp(x), interp(y))
      case e@OrderingLTEQ(x : Exp[T],y : Exp[T]) => e.aev.lteq(interp(x), interp(y))
      case e@OrderingLT(x : Exp[T],y : Exp[T]) => e.aev.lt(interp(x), interp(y))
      case _ => super.interp(d)
  }
}

trait InterpDSLApp extends DSLa with ASTa with InterpNumericExp with InterpOrderingExp with InterpIfThenElseExp {

  final def main(args : Array[String]) {
    println("Hello from DSLApp")
    val x = main()
    globalDefs.map(s => println(s))
    println(interp(x))
  }

  def main() : Rep[Any]

}



object InterpTest extends InterpDSLApp with LiftNumeric {

  def main() = {
    val x : Rep[Double] = 2.0
    val y = x + 4.0
    if (x >= 6.0) y / 25.0 else y / 5.0
  }

  override def compile[A, B](f: (InterpTest.Exp[A]) => InterpTest.Exp[B]): (A) => B = ???
}