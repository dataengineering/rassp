package gr.demokritos.iit.radio.home.protocols

import scala.math._


class EncryptedInt(key : PaillierPublicKey, cypher : BigInt) {

  def times(that : BigInt) : EncryptedInt = key.timesEncrypted(this, that)

  //def plus(y : BigInt) : EncryptedInt

  def plus(that : EncryptedInt) : EncryptedInt = key.plusEncrypted(this, that)

  def toBigInt : BigInt = cypher
}


class PaillierPublicKey(m: BigInt, g : BigInt) {

  val modulusSquared = m * m
  val modulus = m
  val generator = g

  def encrypt(plain : BigInt) : EncryptedInt = {

    val cypher1 = encrypt_without_obfuscate(plain)
    val random = new scala.util.Random()
    val r : BigInt = positiveRandomNumber(modulus, random)
    val cypher2 = (r.modPow(modulus, modulusSquared) * cypher1)  % modulusSquared
    new EncryptedInt(this, cypher2)
  }

  def encrypt_without_obfuscate(plain : BigInt) : BigInt = {
    ((modulus * plain ) + 1)  % modulusSquared
  }

  def positiveRandomNumber(n : BigInt, random : scala.util.Random) : BigInt = {

    val bitlength = n.bitLength

    val r : BigInt = BigInt(bitlength, random)

    if (r >= 1 && r < n) r else positiveRandomNumber(n, random)

  }

  def plusEncrypted(x: EncryptedInt, y: EncryptedInt): EncryptedInt = {
    new EncryptedInt(this, (x.toBigInt * y.toBigInt) % modulusSquared)
  }


  def timesEncrypted(x: EncryptedInt, y : BigInt) : EncryptedInt = {
    new EncryptedInt(this, x.toBigInt.modPow(y, modulusSquared))
  }
}


class PaillierPrivateKey(public : PaillierPublicKey, px : BigInt, qx : BigInt) {

  val p = px
  val q = qx
  val pSquared = p * p
  val qSquared = q * q
  val hp = h(public.generator, p, pSquared)
  val hq = h(public.generator, q, qSquared)
  val pInverse = p.modInverse(q)

  def h(generator : BigInt, x : BigInt, xSquared: BigInt) : BigInt = {
    val y = generator.modPow(x - 1, xSquared)
    L(y, x).modInverse(x)
  }

  def L(u : BigInt, n : BigInt) : BigInt = (u - 1) / n

  def decrypt(cypher : EncryptedInt) : BigInt = {

    val cypherInt : BigInt = cypher.toBigInt
    val decryptedToP = (L(cypherInt.modPow(p-1, pSquared), p) * hp) % p
    val decryptedToQ = (L(cypherInt.modPow(q-1, qSquared), q) * hq) % q
    crt(decryptedToP, decryptedToQ)

  }

  /* chinese remainder theorem */
  def crt(mp : BigInt, mq : BigInt) : BigInt = {
    val u = ((mq - mp)*pInverse) % q
    mp + (u*p)
  }

}

/**
  * Created by angel on 6/2/2016.
  */
class Paillier {


  def keygen(keyLength : Int) : (PaillierPublicKey, PaillierPrivateKey) = {

    val random = new scala.util.Random()

    if (keyLength < 8 || keyLength % 8 != 0)
      throw new IllegalArgumentException("key length must be a multiply of 8")

    val primeLength = keyLength / 2

    /*
    * p, q primes
    * n = p*q
    * g = n + 1
    * lambda = totient = (p-1)*(q-1)
    * mi = totientInverse = totient^{-1} mod n
    * public key  = (n, g)
    * private key = (lambda, mi)
    * */

    val p : BigInt = BigInt.probablePrime(primeLength, random)
    val q : BigInt = BigInt.probablePrime(primeLength, random)

    // n
    val modulus : BigInt = p * q
    // g
    val generator = modulus + 1

    val publicKey = new PaillierPublicKey(modulus, generator)

    val privateKey = new PaillierPrivateKey(publicKey, p, q)
    (publicKey, privateKey)
  }

}


object Paillier extends App {

  val original : BigInt = 99
  val original2 : BigInt = 2
  val scalar : BigInt = 4

  val p = new Paillier()
  val keyPair =  p.keygen(128)

  val encrypted = keyPair._1.encrypt(original)
  //val encrypted2 = keyPair._1.encrypt(original2)

  val encrypted3 = encrypted.times(scalar)

  val decrypted = keyPair._2.decrypt(encrypted3)

  println(s"original = ${scalar*original}, decrypted = $decrypted")
}