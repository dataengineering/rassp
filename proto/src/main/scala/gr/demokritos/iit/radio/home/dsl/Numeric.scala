package gr.demokritos.iit.radio.home.dsl


trait LiftNumeric {
  this : Base =>

  implicit def numericToNumericRep[T:Numeric:Manifest](x : T) : Rep[T] = unit(x)

}

trait NumericOps extends Base {

  implicit def numericToNumericOps[T:Numeric:Manifest](n: T) = new NumericOpsCls(unit(n))
  implicit def repNumericToNumericOps[T:Numeric:Manifest](n: Rep[T]) = new NumericOpsCls(n)

  class NumericOpsCls[T:Numeric:Manifest](lhs: Rep[T]){
    def +[A](rhs: A)(implicit c: A => T) = numeric_plus(lhs,unit(c(rhs)))
    def +(rhs: Rep[T]) = numeric_plus(lhs,rhs)
    def -(rhs: Rep[T]) = numeric_minus(lhs,rhs)
    def *(rhs: Rep[T]) = numeric_times(lhs,rhs)
    def /(rhs: Rep[T]) = numeric_divide(lhs,rhs)
  }

  def numeric_plus[T:Numeric:Manifest](lhs: Rep[T], rhs: Rep[T]) : Rep[T]
  def numeric_minus[T:Numeric:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[T]
  def numeric_times[T:Numeric:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[T]
  def numeric_divide[T:Numeric:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[T]

}


trait NumericOpsExp extends NumericOps with BaseExp {

  abstract class DefMN[A:Manifest:Numeric] extends Def[A] {
    //def mev = manifest[A]
    def aev = implicitly[Numeric[A]]
  }

  case class NumericPlus[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T]
  case class NumericMinus[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T]
  case class NumericTimes[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T]
  case class NumericDivide[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T]


  def numeric_plus[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]) : Exp[T]  = NumericPlus(lhs,rhs)
  def numeric_minus[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[T]  = NumericMinus(lhs,rhs)
  def numeric_times[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[T]  = NumericTimes(lhs,rhs)
  def numeric_divide[T:Numeric:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[T] = NumericDivide(lhs,rhs)

}