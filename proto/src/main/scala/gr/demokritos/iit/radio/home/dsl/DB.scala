package gr.demokritos.iit.radio.home.dsl

trait BaseDB extends Base {

  class DB[T](fields : Vector[String]) {
    override def toString() : String = "DB(" + fields.reduce((x,y) => x + "," + y) + ")"
  }

  object DB {

    def apply[T](field : String) = new DB[T](Vector(field))
    def apply[T](fields : String*): DB[Vector[T]] = new DB[Vector[T]](Vector(fields:_*))

  }

}

trait LiftDB {
  this : BaseDB =>

  implicit def dbToDBRep[T:Manifest](x : DB[T]) : Rep[DB[T]] = unit(x)

}

trait DBOps extends BaseDB {

  class DBOps[T:Manifest](db: Rep[DB[T]]) {
    def rep : Rep[DB[T]] = db
    def map[A:Manifest](f : Rep[T] => Rep[A]) : Rep[DB[A]] = db_map(db, f)
    def filter(f : Rep[T] => Rep[Boolean]) : Rep[DB[T]] = db_filter(db, f)
  }

  def db_map[T:Manifest,A:Manifest](v : Rep[DB[T]], f: Rep[T] => Rep[A]) : Rep[DB[A]]
  def db_filter[T:Manifest](v : Rep[DB[T]], f: Rep[T] => Rep[Boolean]) : Rep[DB[T]]

  implicit def dbToDBOps[T:Manifest](db : Rep[DB[T]]) = new DBOps(db)
  implicit def dbToDBOps[T:Manifest](db : DB[T]) = new DBOps(unit(db))

}


trait DBOpsExp extends DBOps with BaseExp with FunctionsExp {

  case class Map[T:Manifest,A:Manifest](v : Exp[DB[T]], f : Exp[T => A]) extends Def[DB[A]]
  case class Filter[T:Manifest](v : Exp[DB[T]], f : Exp[T] => Exp[Boolean]) extends Def[DB[T]]

  def db_map[T:Manifest,A:Manifest](v : Exp[DB[T]], f : Exp[T] => Exp[A]) : Exp[DB[A]] = Map(v,doLambda(f))
  def db_filter[T:Manifest](v : Exp[DB[T]], f : Exp[T] => Exp[Boolean]) : Exp[DB[T]] = Filter(v,f)

}