package gr.demokritos.iit.radio.home.protocols

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Address, Props, Stash}
import akka.cluster.ClusterEvent.UnreachableMember
import akka.cluster.{Cluster, MemberStatus}
import akka.cluster.client.ClusterClientReceptionist
import akka.routing.{ActorRefRoutee, BroadcastRoutingLogic, Router}

import scala.collection.immutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success}

trait ClusterActor extends Actor {

  val cluster : Cluster = Cluster.get(context.system)

}

trait RASSPActor extends ClusterActor {
  val config : RASSPConfig = RASSPConfig(context.system.settings.config, context.system.name)

}

case object RASSPMessages {

  case class ComputeInstr(instruction : Instruction, replyTo : ActorRef)

  case class ReadyToCompute(worker : ActorRef)

  case class Compute(cluster: IndexedSeq[ActorRef])

}


class ComputeActor extends Actor with ActorLogging with RASSPActor {

  import RASSPMessages._

  def receive : Receive = {

    case ComputeInstr(instr, reply) =>
      log.info(s"Accepting a new computation of instr ${instr} from ${sender()}")


      val reachableNodes = config.GroupProxies.filter(n => n != cluster.selfAddress)
        .filter(n => cluster.state.members.exists(m => n == m.address &&
          m.status == MemberStatus.Up &&
          !cluster.state.unreachable.contains(m)) )

      reachableNodes :+ cluster.selfAddress

      val clusterOk = reachableNodes.nonEmpty

      if (!clusterOk) {
        log.warning("All proxy nodes are unreachable. Cancelling computation")
        sender() ! Failure(new Throwable("All proxy nodes are unreachable. Cancelling computation"))
      } else {
        val nodes = reachableNodes.toSet
        val actor = context.system.actorOf(ComputePerRequestActor.props(nodes, instr, sender()))
        log.info(s"Handling it with ${actor}; will eventually reply to ${sender()}")
      }
  }

}


class GroupComputeActor extends Actor with ActorLogging with RASSPActor {

  import RASSPMessages._

  def receive : Receive = {

    case ComputeInstr(instr, reply) =>
      log.info(s"Accepting a new computation of instr ${instr} from ${sender()}")

      val clusterOk = config.PeerNodes.filter(n => n != cluster.selfAddress)
        .forall(n => cluster.state.members.exists(m => n == m.address &&
                                                       m.status == MemberStatus.Up &&
                                                       !cluster.state.unreachable.contains(m)) )

      if (!clusterOk) {
        log.warning(s"There are unreachable nodes (or not up) in the group. Cancelling computation")
        sender() ! Failure(new Throwable("There are unreachable nodes in the group. Cancelling computation"))
      } else {
        val nodes = config.PeerNodes.toSet.+(cluster.selfAddress)
        val actor = context.system.actorOf(GroupComputePerRequestActor.props(nodes, instr, reply))
        log.info(s"Handling it with ${actor}; will eventually reply to ${sender()}")
        sender() ! ReadyToCompute(actor)
      }
  }

}


class NodeComputeActor extends Actor with ActorLogging {

  import RASSPMessages._

  def receive : Receive = {
    case ComputeInstr(instr, reducer) =>
      log.info(s"Accepting a new computation of instr ${instr} from ${reducer}")

      val workerRef = context.actorOf(RASSPPlayer.props(instr, reducer))
      sender() ! ReadyToCompute(workerRef)
  }
}

object ComputePerRequestActor {

  def props(nodes: Set[Address], instr : Instruction, replyTo : ActorRef) : Props = Props(new ComputePerRequestActor(nodes, instr, replyTo))

}


object GroupComputePerRequestActor {

  def props(nodes: Set[Address], instr : Instruction, replyTo : ActorRef) : Props = Props(new GroupComputePerRequestActor(nodes, instr, replyTo))

}


object SumReducer {

  def props(replyTo : ActorRef) = Props(new SumReducer(replyTo))

}

class SumReducer(val callBack : ActorRef) extends ClusterActor with ActorLogging with Stash
{

  import RASSPMessages._

  case class Collected(shares : Seq[Double])
  case class CollectionFailed(reason : Throwable)

  var store : Map[ActorRef,Double] = HashMap()
  var ack : Set[ActorRef] = Set()

  def shares : Seq[Double] = store.values.toSeq

  override def preStart() : Unit = cluster.subscribe(self, classOf[UnreachableMember])

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive : Receive = waiting

  def waiting : Receive = {

    case Compute(allParties) =>
      log.info(s"Participate in computation of ${allParties.length} groups")
      context.become(collectingOnceFrom(allParties.toSet) orElse collected)
      unstashAll()
    case _ =>
      stash()
  }

  def collectingOnceFrom(peers : Set[ActorRef]) : Receive = {
    case Success(share) =>
      if (!peers.exists(sender.equals(_))) {
        log.warning(s"Share from unexpected peer ${sender}. Known peers ${peers}")
      } else if (ack.contains(sender)) {
        log.warning(s"Duplicate share message received from ${sender}")
      } else {
        store = store + (sender -> share.asInstanceOf[Double])
        ack = ack + sender
      }

      // check whether all shares have been collected
      if (peers forall (ack.contains(_))) {
        log.info("All shares collected")
        self ! Collected(shares)
      }

    case Failure(reason) =>
      if (peers.contains(sender)) {
        // received a legit failure..
        ack = ack + sender

        self ! CollectionFailed(reason)

        if (peers forall (ack.contains(_))) {
          log.info("All shares collected")
          self ! Collected(shares)
        }
      } else {
        // received a non legit failure, warn!
        log.warning(s"Failure from unexpected peer ${sender}")
      }
  }

  def collected : Receive = {
    case Collected(shares) =>
      callBack ! Success(shares.sum)
      log.info(s"Sending success result ${shares.sum} and exiting")
      context.stop(self)
    case CollectionFailed(reason) =>
      log.warning(s"Got failure with reason ${reason}")
      //callBack ! Failure(reason)
      log.warning(s"Sending failure but still continue with others")
    //context.stop(self)
  }

}



class GroupComputePerRequestActor(val nodes: Set[Address], val instr : Instruction, replyTo : ActorRef)
  extends ComputePerRequestActorBase
{
  override val reducerProp: Props = RASSPReducer.props(self)
  val service : String = "computeNode"

  override def receive : Receive = {
    case t : scala.util.Try[Double] =>
      replyTo ! t
      context.stop(self)
    case _ => super.receive
  }
}

class ComputePerRequestActor(val nodes: Set[Address], val instr : Instruction, replyTo : ActorRef)
 extends ComputePerRequestActorBase
{
  override val reducerProp: Props = SumReducer.props(replyTo)
  val service : String = "groupComputeService"
}

trait ComputePerRequestActorBase extends Actor
  with ActorLogging
  with ClusterActor
{

  import RASSPMessages._

  val nodes : Set[Address]
  val instr : Instruction
  val reducerProp : Props
  val service : String

  val peers  = ArrayBuffer.empty[ActorRef]
  var expected : Int = 0

  override def preStart(): Unit = {


    // create a new reducer
    val reducer = context.actorOf(reducerProp) //RASSPReducer.props(replyTo))

    log.info(s"Nodes in cluster $nodes")

    val initialPeers = nodes.map( n => context.actorSelection(s"${n.protocol}://${n.hostPort}/user/${service}") )

    initialPeers.foreach(sel => sel ! ComputeInstr(instr, reducer))

    expected = nodes.size
    context.become(waitForInit(reducer))
  }

  def waitForInit(reducer : ActorRef) : Receive = {

    case ReadyToCompute(worker) => {
      peers += worker

      // FIXME : check if you receive multiple workers from the same node
      expected = expected - 1

      if (expected == 0) {
        startComputation(reducer, peers)
      }
    }
    case Failure(reason)  => {
      log.warning(s"Got failure from ${sender()}")
      expected = expected - 1

      if (expected == 0) {
        startComputation(reducer, peers)
      }
    }
  }

  def startComputation(reducer : ActorRef, peers : Seq[ActorRef]) = {
    reducer ! Compute(peers.toIndexedSeq)
    val routeesSeq = peers.map(x => ActorRefRoutee(x))
    val router = Router(BroadcastRoutingLogic(), routeesSeq.toIndexedSeq)
    router.route(Compute(peers.toIndexedSeq), context.self)
    context.unbecome()
  }

  def receive : Receive = Actor.emptyBehavior

}


/**
  * Created by angel on 4/2/2016.
  */
object RASSP {

  def main(args: Array[String]) : Unit = startUp()

  def startUp(): Unit = {

    val system = ActorSystem("benaloh")

    val receptionist = system.actorOf(Props[ComputeActor], "computeService")
    val groupComputeReceptionist = system.actorOf(Props[GroupComputeActor], "groupComputeService")
    val computeNode = system.actorOf(Props[NodeComputeActor], "computeNode")

    ClusterClientReceptionist(system).registerService(receptionist)
  }

}
