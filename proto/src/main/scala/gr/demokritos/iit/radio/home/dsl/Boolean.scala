package gr.demokritos.iit.radio.home.dsl


trait LiftBoolean {
  this: Base =>

  implicit def boolToBoolRep(b : Boolean) : Rep[Boolean] = unit(b)

}

trait BooleanOps extends Base {
  def infix_unary_!(x: Rep[Boolean]) = boolean_negate(x)
  def infix_&&(lhs: Rep[Boolean], rhs: =>Rep[Boolean]) = boolean_and(lhs,rhs)
  def infix_||(lhs: Rep[Boolean], rhs: =>Rep[Boolean]) = boolean_or(lhs,rhs)


  def boolean_negate(lhs: Rep[Boolean]): Rep[Boolean]
  def boolean_and(lhs: Rep[Boolean], rhs: Rep[Boolean]): Rep[Boolean]
  def boolean_or(lhs: Rep[Boolean], rhs: Rep[Boolean]): Rep[Boolean]
}

trait BooleanOpsExp extends BaseExp {

  case class BooleanNegate(lhs: Exp[Boolean]) extends Def[Boolean]
  case class BooleanAnd(lhs: Exp[Boolean], rhs: Exp[Boolean]) extends Def[Boolean]
  case class BooleanOr(lhs: Exp[Boolean], rhs: Exp[Boolean]) extends Def[Boolean]

  def boolean_negate(lhs: Exp[Boolean]): Exp[Boolean] = BooleanNegate(lhs)
  def boolean_and(lhs: Exp[Boolean], rhs: Exp[Boolean]): Exp[Boolean] = BooleanAnd(lhs,rhs)
  def boolean_or(lhs: Exp[Boolean], rhs: Exp[Boolean]): Exp[Boolean] = BooleanOr(lhs,rhs)

}
