package gr.demokritos.iit.radio.home.dsl

/**
  * Created by angel on 18/4/2016.
  */
trait DBAggrOps extends BaseDB with DBOps {

  class DBNumericOps[T:Numeric:Manifest](db : Rep[DB[T]]) {
    def sum : Rep[T] = db_sum(db)
  }

  def db_sum[T:Numeric:Manifest](v : Rep[DB[T]]) : Rep[T]

  implicit def dbToDBNumericOps[T:Numeric:Manifest](db : Rep[DB[T]]) = new DBNumericOps(db)
  implicit def dbToDBNumericOps[T:Numeric:Manifest](db : DB[T]) = new DBNumericOps(unit(db))
  implicit def dbToDBNumericOps[T:Numeric:Manifest](dbops : DBOps[T]) = new DBNumericOps(dbops.rep)

}


trait DBAggrOpsExp extends DBAggrOps with BaseExp {

  case class Sum[T:Numeric:Manifest](v : Exp[DB[T]]) extends Def[T]

  def db_sum[T:Numeric:Manifest](v: Exp[DB[T]]) : Exp[T] = Sum(v)

}