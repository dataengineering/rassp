package gr.demokritos.iit.radio.home.protocols

import akka.actor._
import akka.cluster.ClusterEvent.{MemberRemoved, UnreachableMember}

import scala.collection.immutable.HashMap
import scala.util.{Failure, Success}
import SimpleSecretSharing._
import akka.cluster.MemberStatus

final case class ShareMsg[Share](s : Share)

trait SecretSharingProtocol[S] {

  type Scheme[S] <: CompositeSecretSharingScheme[S]
  type Secret = S
  type Share = scheme.Share

  val scheme : CompositeSecretSharingScheme[S]

}

trait ShareCollector[S] extends SecretSharingProtocol[S] {
  this : Actor with ActorLogging =>

  case class Collected(shares : Seq[Share])
  case class CollectionFailed(reason : Throwable)

  var store : Map[ActorRef,Share] = HashMap()

  def shares : Seq[Share] = store.values.toSeq

  def collectingOnceFrom(peers : Set[ActorRef]) : Receive = {
    case ShareMsg(share) =>
      if (!peers.contains(sender)) {
        log.warning(s"Share from unexpected peer ${sender}")
      } else if (shares.contains(sender)) {
        log.warning(s"Duplicate share message received from ${sender}")
      } else {
        store = store + (sender -> share.asInstanceOf[Share])
      }

      // check whether all shares have been collected
      if (peers forall (store.contains(_))) {
        log.info("All shares collected")
        self ! Collected(shares)
      }

    case Failure(reason) =>
      if (peers.contains(sender)) {
        // received a legit failure.. exiting
        self ! CollectionFailed(reason)
      } else {
        // received a non legit failure, warn!
        //log.warning(s"Failure from unexpected peer ${sender}")
      }
  }
}


object RASSPReducer {

  def props(replyTo : ActorRef) = Props(new RASSPReducer(replyTo))

}

class RASSPReducer(val callBack : ActorRef) extends ClusterActor with ActorLogging
  with ShareCollector[Double]
{

  import RASSPMessages._

  val scheme = implicitly[CompositeSecretSharingScheme[Double]]

  override def preStart() : Unit = cluster.subscribe(self, classOf[UnreachableMember])

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive : Receive = waiting

  def waiting : Receive = {

    case Compute(allParties) =>
      log.info(s"Participate in computation of ${allParties.length} members")
      context.become(collectingOnceFrom(allParties.toSet) orElse collected)
  }

  def collected : Receive = {
    case Collected(shares) =>
      callBack ! Success(scheme.reconstruct(shares))
      log.info(s"Sending success result ${scheme.reconstruct(shares)} and exiting")
      context.stop(self)
    case CollectionFailed(reason) =>
      log.warning(s"Got failure from ${sender()} with reason ${reason}")
      callBack ! Failure(reason)
      log.info(s"Sending failure and exiting")
      context.stop(self)
  }

}

object RASSPPlayer {
  def props(instr: Instruction, replyTo : ActorRef) = Props(new RASSPPlayer(instr, replyTo))
}

class RASSPPlayer(val instr : Instruction, val callBack : ActorRef) extends Actor
  with ShareCollector[Double]
  with akka.actor.ActorLogging
  with ClusterActor
  with Stash
{

  import RASSPMessages._

  val evaluator : InstructionEvaluator = new InstructionEvaluator
  val scheme = implicitly[CompositeSecretSharingScheme[Double]]

  val minimum_size = 3

  def generateShares(count : Int)(secret: Double) : Seq[Share] = {
    scheme.split(count)(secret)
  }

  def distributeShares(all : IndexedSeq[ActorRef], shares : Seq[Share]) : Unit = {
    (all, shares).zipped.foreach((a,ss) => a ! ShareMsg(ss))
  }

  def receive : Receive = waiting

  def waiting : Receive = {

    case Compute(allParties) =>

      if (!allParties.contains(self))
        replyFailureAndStop("Computation does not contain myself")

      if (!allParties.filter( a => ! a.equals(self) ).forall( a => cluster.state.members
                              .exists(m => m.address.equals(a.path.address) && m.status.equals(MemberStatus.Up) &&
                                !cluster.state.unreachable.contains(m))))
        replyFailureAndStop(s"Refuse to start computation until member ${cluster.state.unreachable} become reachable again")
      else if (allParties.length < minimum_size)
        replyFailureAndStop(s"Refuse to start computation using less than $minimum_size peers (current cluster size = ${allParties.length})")

      context.become(
        collectingOnceFrom(allParties.toSet) orElse
          collected orElse
          clusterMonitor(allParties.toSet))

      unstashAll()

      if (allParties.contains(context.self)) {
        val value = evaluator.evaluate(instr)
        //log.info(s"Evaluated value = ${value}")

        log.info("Distribute shares to other peers")
        distributeShares(allParties, generateShares(allParties.length)(value))
        //log.debug(s"Start with ${all.length} other parties")
      }
    case msg =>
      log.warning(s"Receive message ${msg}, stashing...")
      stash()
  }

  def collected : Receive = {
    case Collected(shares) =>
      callBack ! ShareMsg(shares.reduceLeft(scheme.combineShares))
      context.stop(self)
    case CollectionFailed(reason) =>
      replyFailureAndStop(reason)
  }

  def replyFailureAndStop(reason : Throwable): Unit = {
    callBack ! Failure(reason)
    context.stop(self)
  }

  def replyFailureAndStop(msg : String): Unit = replyFailureAndStop(new Throwable(msg))

  def clusterMonitor(peers : Set[ActorRef]) : Receive = {
    case UnreachableMember(member) =>
      if (peers.map(a => a.path.address).contains(member.address))
        replyFailureAndStop(s"Cancel computation, node ${member.address} unreachable.")
    case MemberRemoved(member,_) =>
      if (peers.map(a => a.path.address).contains(member.address))
        replyFailureAndStop(s"Cancel computation, node ${member.address} unreachable.")
  }

}
