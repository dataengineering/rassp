package gr.demokritos.iit.radio.home.dsl

/**
  * Created by angel on 26/2/2016.
  */

trait Base  extends scala.EmbeddedControls {

  type Rep[+T]

  protected def unit[T:Manifest](x : T) : Rep[T]

  implicit def unitToRepUnit(x: Unit) = unit(x)

  implicit def unitToRepNull(x: Null) = unit(x)

}


trait Compile extends Base {

  def compile[A,B](f: Rep[A] => Rep[B]) : A => B

}


object Test extends DSLApp with LiftNumeric {

  //implicit def doubleToRepDouble(x: Double) = unit(x)

  def main() {
    val x : Rep[Double] = 2 + 3
    val z = x + 4
    val y = if (x >= z) 1.0 + x else 2.0
    //val z = y + 1
    val i = y +  2.4
  }

}


