package gr.demokritos.iit.radio.home.protocols

/**
  * @tparam S elements in the domain of secrets
  *
  */
trait SecretSharingScheme[S]  {

  type Secret = S
  type Share

  /**
    * Reconstruction of a secret given a set of shares
    *
    * @param shares a set of shares
    * @return the recostructed secret
    */
  def reconstruct(shares : Seq[Share]) : S

  /**
    * Splits a secret S into k shares
    *
    * @param secret the secret
    * @return
    */
  def split(k: Int)(secret : S) : Seq[Share]


  trait SecretSharingSchemeLaw {
    // secret = fI(shares(k)(secret))
  }

}

trait CompositeSecretSharingScheme[S] extends SecretSharingScheme[S] {


  /*
  * a combination of shares is also a share
  */
  def combineShares(share1: Share, share2: Share) : Share

  trait CompositeSecretSharingSchemeLaw {
    // secret = fI(shares(k)(secret))
  }

}


object CompositeSecretSharingSchemeImplicits {


  implicit def sharingschemetoSeq[Secret](implicit csss : CompositeSecretSharingScheme[Secret]) :
    CompositeSecretSharingScheme[Seq[Secret]] = new CompositeSecretSharingSchemeSeq(csss)


  class CompositeSecretSharingSchemeSeq[S](val scheme : CompositeSecretSharingScheme[S])
      extends CompositeSecretSharingScheme[Seq[S]]
  {
    type Share = Seq[scheme.Share]
    /*
      * a combination of shares is also a share
      */
    def combineShares(share1: Share, share2: Share): Share = (share1, share2).zipped.map(scheme.combineShares)

    /**
      * Reconstruction of a secret given a set of shares
      *
      * @param shares a set of shares
      * @return the recostructed secret
      */
    def reconstruct(shares: Seq[Share]): Secret =  {

      def flipSeq[T](s : Seq[Seq[T]]) : Seq[Seq[T]] = {
        val firsts  = s.flatMap(_.headOption)
        val rest = flipSeq(s.map(_.tail))
        Seq[Seq[T]](firsts) ++ rest
      }

      flipSeq(shares).map(scheme.reconstruct)
    }

    /**
      * Splits a secret S into k shares
      *
      * @param secret the secret
      * @return
      */
    def split(k: Int)(secret: Secret) : Seq[Share] = secret.map(s => scheme.split(k)(s))

  }

}
