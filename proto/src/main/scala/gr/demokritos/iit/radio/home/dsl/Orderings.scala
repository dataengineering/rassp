package gr.demokritos.iit.radio.home.dsl


trait OrderingOps extends Base {

  implicit def orderingToOrderingOps[T:Ordering:Manifest](n: T) = new OrderingOpsCls(unit(n))
  implicit def repOrderingToOrderingOps[T:Ordering:Manifest](n: Rep[T]) = new OrderingOpsCls(n)

  class OrderingOpsCls[T:Ordering:Manifest](lhs: Rep[T]){
    def <       (rhs: Rep[T]) = ordering_lt(lhs, rhs)
    def <=      (rhs: Rep[T]) = ordering_lteq(lhs, rhs)
    def >       (rhs: Rep[T]) = ordering_gt(lhs, rhs)
    def >=      (rhs: Rep[T]) = ordering_gteq(lhs, rhs)
    def equiv   (rhs: Rep[T]) = ordering_equiv(lhs, rhs)

    def <       [B](rhs: B)(implicit c: B => Rep[T]) = ordering_lt(lhs, c(rhs))
    def <=      [B](rhs: B)(implicit c: B => Rep[T]) = ordering_lteq(lhs, c(rhs))
    def >       [B](rhs: B)(implicit c: B => Rep[T]) = ordering_gt(lhs, c(rhs))
    def >=      [B](rhs: B)(implicit c: B => Rep[T]) = ordering_gteq(lhs, c(rhs))
    def equiv   [B](rhs: B)(implicit c: B => Rep[T]) = ordering_equiv(lhs, c(rhs))
  }

  def ordering_lt      [T:Ordering:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[Boolean]
  def ordering_lteq    [T:Ordering:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[Boolean]
  def ordering_gt      [T:Ordering:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[Boolean]
  def ordering_gteq    [T:Ordering:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[Boolean]
  def ordering_equiv   [T:Ordering:Manifest](lhs: Rep[T], rhs: Rep[T]): Rep[Boolean]

}


trait OrderingOpsExp extends BaseExp {

  abstract class DefMN[T:Manifest:Ordering, A:Manifest] extends Def[A] {
    //def mev = manifest[A]
    def aev = implicitly[Ordering[T]]
  }

  case class OrderingLT      [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T, Boolean]
  case class OrderingLTEQ    [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T, Boolean]
  case class OrderingGT      [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T, Boolean]
  case class OrderingGTEQ    [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T, Boolean]
  case class OrderingEquiv   [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]) extends DefMN[T, Boolean]

  def ordering_lt      [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[Boolean] = OrderingLT(lhs,rhs)
  def ordering_lteq    [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[Boolean] = OrderingLTEQ(lhs,rhs)
  def ordering_gt      [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[Boolean] = OrderingGT(lhs,rhs)
  def ordering_gteq    [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[Boolean] = OrderingGTEQ(lhs,rhs)
  def ordering_equiv   [T:Ordering:Manifest](lhs: Exp[T], rhs: Exp[T]): Exp[Boolean] = OrderingEquiv(lhs,rhs)

}