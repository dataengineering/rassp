package gr.demokritos.iit.radio.home.stats

import gr.demokritos.iit.radio.home.dsl.{LiftNumeric, DSLApp}

/**
  * Created by angel on 11/4/2016.
  */
object LinearRegression extends DSLApp with LiftNumeric {


  def main() : Unit = {

    /*
    val m1 = db.sum(x => x)
    val m2 = db.sum(y => y)

    val xp = db().sum(x => x - m1)
    val yp = db().sum(x => y - m2)
    */


    //val db = DB[Double]("bloodPressure")

    //val y = x.map(x => if (x >= 0.0) 0.0 else 2.0).sum

    val y = DB[Double]("bloodPressure").sum
    val ny = DB[Double]("bloodPressure").map(x => 1.0).sum
    val avgy = y / ny
    val y2 = DB[Double]("bloodPressure").map(x => (x - avgy) / 2.0).sum

  }

}
