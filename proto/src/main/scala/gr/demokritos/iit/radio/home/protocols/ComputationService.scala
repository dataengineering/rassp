package gr.demokritos.iit.radio.home.protocols

import akka.actor.{ActorSystem, TypedActor, TypedProps}
import akka.cluster.client.{ClusterClient, ClusterClientSettings}
import akka.event.Logging
import akka.pattern.ask

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import scala.concurrent.duration._

trait ComputationService {

  def computeAsync(instr: Instruction) : Future[Double]

}

object ComputationService {

  def apply(system : ActorSystem) : ComputationService = {
    TypedActor(system).typedActorOf(TypedProps[ComputationServiceImpl])
  }

}

class ComputationServiceImpl extends ComputationService  {

  implicit val ec = ExecutionContext.Implicits.global
  implicit val timeout = 100 second

  import RASSPMessages._

  val ctx = TypedActor.context

  val log = Logging(TypedActor.context.system, TypedActor.context.self)

  def computeAsync(instr: Instruction) : Future[Double] = {
    val client = ctx.actorOf(ClusterClient.props(ClusterClientSettings(ctx.system)))
    client.ask(ClusterClient.Send("/user/computeService", ComputeInstr(instr, client.actorRef), false))(timeout) flatMap {
      case res : scala.util.Try[_] =>
        ctx.stop(client)
        Future.fromTry(res.asInstanceOf[scala.util.Try[Double]])
    }
  }

}
