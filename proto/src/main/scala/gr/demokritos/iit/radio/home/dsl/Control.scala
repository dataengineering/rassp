package gr.demokritos.iit.radio.home.dsl


trait IfThenElse extends Base {

  def __ifThenElse[T:Manifest](cond : Rep[Boolean], thenp: => Rep[T], elsep: => Rep[T]) : Rep[T]

  override  def __ifThenElse[T](cond: =>Boolean, thenp: => T, elsep: => T) = cond match {
    case true => thenp
    case false => elsep
  }
}

trait IfThenElseExp extends BaseExp {
  case class IfThenElse[T:Manifest](cond : Exp[Boolean], thenp : Exp[T], elsep : Exp[T]) extends Def[T]

  def __ifThenElse[T:Manifest](cond : Exp[Boolean], thenp: => Exp[T], elsep: => Exp[T]) : Exp[T] = IfThenElse(cond,thenp,elsep)

}
