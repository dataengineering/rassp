package gr.demokritos.iit.radio.home.protocols


import gr.demokritos.iit.radio.home.qfr.QfrMessage
import scala.collection.JavaConversions._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import java.io.File

class Instruction
case class SumInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction
case class SquareInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction
case class ParticipationInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction
case class ThirdPowerInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction
case class ForthPowerInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction
case class MultiplyInstruction(attrib: java.util.List[AnyRef], params: java.util.List[AnyRef]) extends Instruction


class InstructionEvaluator {

  val data : scala.collection.mutable.Map[String,Any]  = collection.mutable.Map[String, Any]()

  loadData();

  def evaluate(instr : Instruction) : Double = {
    instr match  {
      case SumInstruction(attr, params) =>
        evaluateSum(attr.toList, params.toList)
      case SquareInstruction(attr, params) =>
        evaluateSquare(attr.toList, params.toList)
      case ParticipationInstruction(attr, params) =>
        evaluateParticipation(attr.toList, params.toList)
      case ThirdPowerInstruction(attr, params) =>
        evaluateThirdPow(attr.toList, params.toList)
      case ForthPowerInstruction(attr, params) =>
        evaluateForthPow(attr.toList, params.toList)
      case MultiplyInstruction(attr, params) =>
        evaluateMultiply(attr.toList, params.toList)
      case _ =>
        0.0
    }
  }

  def evaluateSum(attrib: List[AnyRef], params: List[AnyRef]): Double = {
    val v1 = getData(attrib)
    val v = if(v1 != Double.MaxValue) v1 else 0.0
    v
  }

  def evaluateSquare(attrib: List[AnyRef], params: List[AnyRef]): Double = {
    val p = if (params.nonEmpty) params.head.asInstanceOf[Double] else 0.0
    val v1 = getData(attrib)
    val v = if (v1 != Double.MaxValue) v1 - p else 0.0
    v * v
  }

  def evaluateMultiply(attrib: List[AnyRef], params: List[AnyRef]): Double = {

    attrib.zip(params).map({ case (a,p) =>
      if(getData(List(a)) != Double.MaxValue)
        getData(List(a)) + p.asInstanceOf[Double]
      else 1.0
    }).product

  }

  def evaluateParticipation(attrib: List[AnyRef], params: List[AnyRef]): Double = {
    val v1 = getFilter(attrib)
    if (v1) 1.0 else 0.0

  }

  def evaluateThirdPow(attrib: List[AnyRef], params: List[AnyRef]): Double = {
    val p = if (params.nonEmpty) params.head.asInstanceOf[Double] else 0.0
    val v1 = getData(attrib)
    val v = if (v1 != Double.MaxValue) v1 - p else 0.0
    v * v * v
  }

  def evaluateForthPow(attrib: List[AnyRef], params: List[AnyRef]): Double = {
    val p = if (params.nonEmpty) params.head.asInstanceOf[Double] else 0.0
    val v1 = getData(attrib)
    val v = if (v1 != Double.MaxValue) v1 - p else 0.0
    v * v * v * v
  }

  def getData(attrib : List[AnyRef]) : Double = {

    if (attrib.isEmpty)
      return Double.MaxValue

    val msg: QfrMessage = attrib.head.asInstanceOf[QfrMessage]

    if (msg.getSelectClause.isEmpty)
      return Double.MaxValue


    val name: String = msg.getSelectClause.head.asInstanceOf[String]

    if (getFilter(attrib)) {
      val v = getData(name)
      v
    }
    else
    //0.0
      Double.MaxValue
  }

  def getFilter(attrib : List[AnyRef]) : Boolean = {

    if (attrib.isEmpty)
      return false

    val msg : QfrMessage = attrib.head.asInstanceOf[QfrMessage]

    val conditions = msg.getTriples.map(v => (v.get(0).toString, v.get(1).toString, v.get(2).toString))

    if(conditions.isEmpty) {
      val condition = msg.getSelectClause.head.asInstanceOf[String]
      val result = exist(condition)

      return result
    }
    else {
      val result = satisfy(conditions.toList)

      return result
    }

  }

  def satisfy(conds : List[(String,String,String)]): Boolean = conds.forall(satisfy)

  def exist(conds : List[(String)]): Boolean = conds.forall(exist)

  def exist(conds : String) : Boolean = {
    val dv = data.get(conds)
    dv match {
      case Some(dvv) => true
      case None => false
    }
  }

  def getData(name : String) : Double = {
    data.getOrElse(name, Double.MaxValue).asInstanceOf[Double]
  }

  def satisfy(cond : (String, String, String)) : Boolean =  {
    val dv = data.get(cond._1)
    dv match {
      case Some(dvv) =>
        cond._2 match {
          case "=" => dvv.equals(cond._3)
          case _ => false
        }
      case None => false
    }
  }

  def loadData(): Unit = {
    //data += ("bl_pressure" -> 2.0 * playerNumber)
    //data += ("med" -> (if (playerNumber <= 4) "A" else "B"))
    // FIXME load from file using playerNumber
    //val jsonStr = "{ \"bl_pressure\": 23 , \"med\": \"A\"  }"
    val file = getFileForActor

    val jsonStr = scala.io.Source.fromFile(file).mkString

    implicit val  ff  = org.json4s.DefaultFormats
    data ++= parse(jsonStr).extract[Map[String,Any]]
  }

  def getFileForActor: String = System.getProperty("db.file")

}