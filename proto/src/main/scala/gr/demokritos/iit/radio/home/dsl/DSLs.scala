package gr.demokritos.iit.radio.home.dsl


trait DSLa extends Base with NumericOps with BooleanOps with IfThenElse with OrderingOps with DBOps with DBAggrOps with Functions

trait ASTa extends BaseExp with NumericOpsExp with BooleanOpsExp with IfThenElseExp with OrderingOpsExp with DBOpsExp with DBAggrOpsExp with FunctionsExp

trait DSLApp extends DSLa with ASTa {

  final def main(args : Array[String]) {
    println("Hello from DSLApp")
    val x = main()
    globalDefs.map(s => println(s))
  }

  def main() : Unit

}


