package gr.demokritos.iit.radio.home.dsl

import scala.collection.mutable.ListBuffer
import scala.annotation.unchecked.uncheckedVariance


/**
 * Created by angel on 11/4/2016.
 */

trait Expressions {

  abstract class Exp[+T:Manifest] {
    def tp : Manifest[T @uncheckedVariance] = manifest[T]
  }

  case class Const[+T:Manifest](c: T) extends Exp[T]

  abstract class Def[+T:Manifest]

  case class Sym[+T:Manifest](val id : Int) extends Exp[T]

  var nVars = 0


  abstract class Stm

  case class TP[+T](sym : Sym[T], rhs: Def[T]) extends Stm

  var globalDefs : List[Stm] = Nil

  def fresh[T:Manifest] : Sym[T] = { nVars+=1; Sym(nVars) }


  def infix_defines[A](stm : Stm, sym : Sym[A]) : Option[Def[A]] = stm match {
    case TP(`sym`, rhs : Def[A]) => Some(rhs)
    case _ => None
  }

  def infix_defines[A](stm: Stm, rhs: Def[A]): Option[Sym[A]] = stm match {
    case TP(sym: Sym[A], `rhs`) => Some(sym)
    case _ => None
  }


  def infix_lhs(stm: Stm): List[Sym[Any]] = stm match {
    case TP(sym, rhs) => sym::Nil
  }

  def infix_rhs(stm: Stm): Any = stm match { // clients use syms(e.rhs), boundSyms(e.rhs) etc.
    case TP(sym, rhs) => rhs
  }

  def createDefinition[T](s : Sym[T], d : Def[T]) : Stm = {
    val f = TP(s, d)
    globalDefs = globalDefs :+ f
    f
  }

  def findDefinition[T](s: Sym[T]): Option[Stm] =
    globalDefs.find( s1 => s1 match {
      case TP(`s`, _) => true
      case _ => false
    })

  def findDefinition[T](d : Def[T]) : Option[Stm] = {
    globalDefs.find(s => s.defines(d).nonEmpty)
  }

  def findOrCreateDefinition[T:Manifest](d : Def[T]) : Stm = {
    findDefinition(d).getOrElse(createDefinition(fresh[T], d))
  }

  def findOrCreateDefinitionExp[T:Manifest](d : Def[T]) : Exp[T] = {
    findOrCreateDefinition(d).defines(d).get
  }

  protected implicit def toAtom[T:Manifest](d: Def[T]): Exp[T] = {
    findOrCreateDefinitionExp(d)
  }

  object Def {
    def unapply[T](e: Exp[T]) : Option[Def[T]] = e match {
      case s@Sym(_) => findDefinition(s).flatMap(_.defines(s))
      case _ => None
    }
  }

}

trait BaseExp extends Base with Expressions {

  type Rep[+T] = Exp[T]

  protected def unit[T:Manifest](x : T) : Rep[T] = Const(x)

}

