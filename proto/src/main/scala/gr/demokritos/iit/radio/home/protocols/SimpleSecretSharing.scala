package gr.demokritos.iit.radio.home.protocols

/**
  * (k, n) :
  * D : the domain of secrets
  * (+) : D x D -> D : the binary function over D
  * T : the domain of shares
  * (x) : T x T -> T : the binary function over T
  * fI : T^k -> D : the k-nary functions from T to D
  *
  */
object SimpleSecretSharing {

  import Random._

  class SimpleSecretSharingScheme[T : Numeric : Random] extends SecretSharingScheme[T] {

    type Share = T

    val num : Numeric[T] = implicitly[Numeric[T]]

    val random : Random[T] = implicitly[Random[T]]

    /* generates k-1 random integers and the k-st is the secret minus the sum of those numbers */
    def split(k: Int)(secret: T): Seq[Share] = {
      val vv = Seq.fill(k - 1) (random.randomNext())
      vv :+ num.minus(secret, vv.reduceLeft(num.plus))
    }

    /* if all k shares are in hand them suming them will remove the random numbers and reveal the secret */
    def reconstruct(shares: Seq[Share]) : Secret = shares.reduceLeft(num.plus)

  }

  class CompositeSimpleSecretSharingScheme[T : Numeric : Random] extends SimpleSecretSharingScheme[T] with CompositeSecretSharingScheme[T] {

    def combineShares(share1: Share, share2: Share) : Share = num.plus(share1,share2)

  }

  implicit def compositeSimpleSecretShare[T : Numeric : Random]() : CompositeSecretSharingScheme[T] = new CompositeSimpleSecretSharingScheme[T]()
  implicit def doubleCompositeSimpleSecretShare : CompositeSecretSharingScheme[Double] = compositeSimpleSecretShare()
  implicit def intCompositeSimpleSecretShare : CompositeSecretSharingScheme[Int]    = compositeSimpleSecretShare()
}