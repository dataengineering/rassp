package gr.demokritos.iit.radio.home.dsl


trait Functions extends Base {

  def doLambda[A:Manifest,B:Manifest](fun : Rep[A] => Rep[B]) : Rep[A => B]

  implicit def fun[A:Manifest,B:Manifest](f : Rep[A] => Rep[B]) : Rep[A => B] = doLambda(f)

  /*
  class LambdaOps[A,B](f : Rep[A => B]) {
    def apply(x : Rep[A]) : Rep[B] = doApply(f, x)
  }

  implicit def toLambdaOps[A,B](fun : Rep[A => B])

  def doApply[A,B](fun : Rep[A => B], x : Rep[A]) : Rep[B]
  */
}

trait FunctionsExp extends BaseExp {

  case class Lambda[A:Manifest,B:Manifest](x : Exp[A], y : Exp[B]) extends Def[A => B]

  def doLambda[A:Manifest,B:Manifest](fun : Exp[A] => Exp[B]) : Exp[A => B] = {
    val x = fresh[A]
    val y = fun(x)
    Lambda(x, y)
  }

}
