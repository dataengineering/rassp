package gr.demokritos.iit.radio.home.protocols

import scala.math.BigInt

/**
  * Created by angel on 6/4/2017.
  */
object Random {

  type Random[T] = RandomOps[T]

  trait RandomOps[T] {

    def randomNext(): T
    def randomNext(max : T) : T
    def randomNextDefault(max : T)(implicit ord : Ordering[T]) : T = {
      var t = randomNext()
      while (ord.gt(t, max)) {
        t = randomNext()
      }
      t
    }
  }

  class IntRandom(val r : scala.util.Random) extends RandomOps[Int] {
    def randomNext() : Int = r.nextInt()
    def randomNext(max : Int) : Int = r.nextInt(max)
  }

  class BigIntRandom(val r : scala.util.Random) extends RandomOps[BigInt] {
    val defaultBitLength = 128

    def randomNext() : BigInt = BigInt(defaultBitLength, r)

    def randomNext(max : BigInt) : BigInt = {
      val i = BigInt(max.bitLength, r)
      if (i <= max) i else randomNext(max)
    }
  }

  class DoubleRandom(val r : scala.util.Random) extends RandomOps[Double] {
    def randomNext() : Double = r.nextDouble()
    def randomNext(max : Double) : Double = randomNextDefault(max)
  }

  class FloatRandom(val r : scala.util.Random) extends RandomOps[Float] {
    def randomNext() : Float = r.nextFloat()
    def randomNext(max : Float) : Float = randomNextDefault(max)
  }

  implicit val r : scala.util.Random = new scala.util.Random()

  implicit def intRandom(implicit r : scala.util.Random) : Random[Int] = new IntRandom(r)
  implicit def bigIntRandom(implicit r : scala.util.Random) : Random[BigInt] = new BigIntRandom(r)
  implicit def doubleRandom(implicit r : scala.util.Random) : Random[Double] = new DoubleRandom(r)
  implicit def floatRandom(implicit r : scala.util.Random) : Random[Float] = new FloatRandom(r)

}