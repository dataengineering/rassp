package gr.demokritos.iit.radio.home.protocols

import scalaz._
import Scalaz._

/*
 * http://cs.jhu.edu/~sdoshi/crypto/papers/shamirturing.pdf
 *
 * */
object ShamirsSecretSharing {


  class ShamirsSecretSharingScheme(val random : scala.util.Random) extends SecretSharingScheme[Int] {

    type Share = (Int, Int)

    val p = 257

    def split(k: Int)(secret: Int) : Seq[Share] = {

      val coef = secret +: Seq.fill(k - 1) {
        random.nextInt(1000)
      }

      val xs = 1 to k

      def c(x: Int) =
        coef.zipWithIndex
          .foldLeft(0)({ case (acc, (c, e)) => (acc + (c * (scala.math.pow(x, e) % p).toInt) % p) % p })

      xs.zip(xs.map(c))
    }

    def reconstruct(shares: Seq[Share]): Int = {
      val xs = shares.map(_._1)
      val ys = shares.map(_._2)

      def l(x: Int): Int = {
        val nxs = xs.filterNot(_ == x)
        val nominator = nxs.map(-_).product % p
        val denominator = nxs.map(x - _).product % p
        nominator * modInverse(denominator)
      }

      shares
        .map({ case (x, y) => y * l(x) })
        .foldLeft(0)({ case (acc, x) => (acc + x) % p })
    }


    def gcdD(a: Int, b: Int): (Int, Int, Int) = {
      if (b == 0) (a, 1, 0)
      else {
        val n = scala.math.floor(a / b)
        val c = a % b
        val r = gcdD(b, c)
        (r._1, r._3, r._2 - (r._3 * n).toInt)
      }
    }

    def modInverse(k: Int): Int = {
      val kk: Int = k % p
      val r: Int = if (kk < 0) -gcdD(p, -kk)._3 else gcdD(p, kk)._3
      (p + r) % p
    }

  }

  class CompositeShamirsSecretSharingScheme(random : scala.util.Random) extends ShamirsSecretSharingScheme(random) with CompositeSecretSharingScheme[Int] {

    def combineShares(share1: Share, share2: Share) : Share = (share1._1 + share2._1, share1._2 + share2._2)

  }

  implicit def shamirsScheme(r : scala.util.Random) : CompositeSecretSharingScheme[Int] = new CompositeShamirsSecretSharingScheme(r)
}


object ShamirsSecretSharingTest extends App {

  import ShamirsSecretSharing._

  val random : scala.util.Random = new java.security.SecureRandom()
  val scheme : ShamirsSecretSharingScheme = new ShamirsSecretSharingScheme(random)

  val secret1 = 70
  val secret2 = 50

  val shares1 = scheme.split(5)(secret1)
  val shares2 = scheme.split(5)(secret2)

  val shares = (shares1, shares2).zipped.map((x,y) => x mappend y)

  val secret = scheme.reconstruct(shares)

  println(s"secret1 = ${secret1}, secret2 = ${secret2}, recostructed = ${secret}")
}
