package gr.demokritos.iit.radio.home.dsl


trait VectorOps extends Base {

  def vec_length[T](v : Rep[Vector[T]]) : Rep[Int]
  def vec_map[T,A](v : Rep[Vector[T]], f: Rep[T] => Rep[A]) : Rep[Vector[A]]
  def vec_reduce[T,A](v : Rep[Vector[T]], r : Rep[A] => Rep[T] => Rep[A]) : Rep[A]

  def vec_plus[T:Numeric](lhs : Rep[Vector[T]], rhs : Rep[Vector[T]]) : Rep[Vector[T]]
  def vec_scale[T:Numeric](v : Rep[Vector[T]], a : Rep[T]) : Rep[Vector[T]]

}