package gr.demokritos.iit.radio.home.api;

import gr.demokritos.iit.radio.home.qfr.WhereClause;

import java.util.List;

/**
 * QfrConverter
 * <p>Class description
 *
 * @author Katerina Zamani
 */

public interface QfrConverter {

    void convertSelect(List<Object> selectClause);

    void convertWhere(List<WhereClause> whereClause);

    String getQuery();
}
