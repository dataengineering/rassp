package gr.demokritos.iit.radio.home.qfr;

import gr.demokritos.iit.radio.home.api.QfrConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * QfrMessage
 * <p> The format of a query as it is specified from the statistician via R language.
 *
 * @author Katerina Zamani
 */

public class QfrMessage implements java.io.Serializable  {

    private static final long serialVersionUID = 1L;

    private List<Object> selectClause = new ArrayList<>();
    //not used ?!?!
    private List<WhereClause> whereClauses = new ArrayList<>();

    ////////// It defines the whereClauses as triples - <var> <op> <value>
    private List<Vector<Object>> triples = new ArrayList<>();

    private QfrConverter converter;

    public QfrMessage(QfrConverter converter) {
        this.converter = converter;
    }

    public QfrMessage() {
    }

    public QfrMessage(String select) {
        this.selectClause.add(select);
    }

    public QfrMessage(List<Object> select,  List<Vector<Object>> triples) {
        this.selectClause = select;
        this.triples = triples;
    }

    public void setSelect(ArrayList<Object> selectClause) {
        this.selectClause = selectClause;
    }

    public void addSelect(Object select) { this.selectClause.add(select); }

    public void setWhere(ArrayList<WhereClause> whereClause) {
        this.whereClauses = whereClause;
    }

    public List<Object> getSelectClause() {
        return selectClause;
    }

    public List<WhereClause> getWhereClauses() {
        return whereClauses;
    }

    public List<Vector<Object>> getTriples() {
        return triples;
    }

    public void setTriples(List<Vector<Object>> triples) {
        this.triples = triples;
    }

    public void addTriple(Vector<Object> vect) {
        triples.add(vect);
    }

    public void build() {
        converter.convertSelect(selectClause);
        converter.convertWhere(whereClauses);
    }

    public String getMesasage() {
        return converter.getQuery();
    }

    @Override
    public String toString() {
        String str = "SELECT ";

        for(Object s : selectClause) {
            str = str + s + ", ";
        }
        str = str +"\nWHERE ";


        for(List tList : triples) {
            for(Object triples : tList ) {
                str = str + triples.toString() +" ";
            }
            str = str +" and ";
        }

        str = str.substring(0, str.length()-4);

        return str;

    }

    /**
     * The query should be transformed in a suitable format that fits the database.
     **/
    public void convert(String type) {
        // single attributes in the DB: <selectClause>_<conditionName>_<conditionValue>
        if(type.equalsIgnoreCase("RepeatedMeasuresAnova")) {
            String select = (String) selectClause.get(0)+"_";
            Vector<Object> triple = getTriples().get(0);
            select = select + triple.get(0) + "_" + triple.get(2);

            this.triples = new ArrayList<>();
            this.selectClause = new ArrayList<>();
            this.selectClause.add(select);
        }
    }

}
