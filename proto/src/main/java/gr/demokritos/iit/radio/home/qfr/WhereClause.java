package gr.demokritos.iit.radio.home.qfr;

import java.util.ArrayList;
import java.util.List;

/**
 * WhereClause
 * <p>
 * Class description
 *
 * @author Katerina Zamani
 */

public class WhereClause {

    private List<String> variables;
    private List<String> operators;
    private List<Object> values;

    public WhereClause() {
        this.values = new ArrayList<>();
        this.variables = new ArrayList<>();
        this.operators = new ArrayList<>();
    }

    public void setVariables(List<String> vars) { this.variables = vars; }

    public void addVariable(String var) { this.variables.add(var); }

    public void setOperators(List<String> operators) { this.operators = operators; }

    public void addOperator(String op) { this.operators.add(op); }

    public void setValues(List<Object> values) { this.values = values; }

    public void addValue(Object val) { this.values.add(val); }

    public List getVariables() { return variables; }

    public List getOperators() { return operators; }

    public List getValues() { return values; }


}
