package gr.demokritos.iit.radio.home.entities;

/**
 * Message Type
 * 
 * <p>Denotes the type of the equation the system should build, in order to be executed as an Instruction
 * for the final statistic computation.</p>
 *  
 * @author Katerina Zamani
 */


public enum MessageType {

    TROWNMULTIPLY,

    /////////////////////////////// For Secure Statistics ///////////////////
    SUM,                 // x
    SQUARE,              // (x - a)^2
    THIRDPOWER,          // (x - a)^3
    FORTHPOWER,          // (x - a)^4
    MULTIPLY,            // (x + a1) * ... *  (y + a2)
    PARTICIPATION        // if ( x1 op a1 and ... and xn op an) then 1 else 0

}

