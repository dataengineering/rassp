package gr.demokritos.iit.radio.home.statistics;

import Jama.Matrix;
import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * MultipleLinearRegression
 *
 * <p>Linear regression for more than one independent variables. We use the Ordinary Least Squares technique for coefficient's estimation, in order to preserve privacy.
 * Line : y_i = x_i^T * β + ε_i , where i = 1 ... n
 * β = Σ(x_i * x_i^T) * Σ(x_i * y_i)
 *
 * @author Katerina Zamani
 */

public class MultipleLinearRegression {

    private int state = 0;

    private Matrix xxT;         /* Σ(x_i * x_i^T) */
    private Matrix xy;          /* Σ(x_i * y_i) */
    private int n;
    private int p;

    // for fitting
    private Matrix coefficients;      /* denotes the slopes of the line */
    //private Matrix residual;             /* denotes the residual distribution */

    // for analysis
    //private double R2 = 0.0;        /* the proportion of the variance in the y variable that is predictable from the x variables */
    private double s2 = 0.0;        /* OLS estimate */
    private double sigma2 = 0.0;
    //private double Sy = 0.0;        /* standard deviation of y */

    private List<Instruction> instructions = new ArrayList<>();

    public MultipleLinearRegression(String y, Vector<String> x) {
        p = x.size();
        init(x, y);
    }


    private void init(Vector<String> x, String y) {

        Instruction instr;

        //1st instr: Sum x_i * x_i^T -> x_i should be represented as a vector , i.e column matrix
        instr = new Instruction(MessageType.TROWNMULTIPLY);
        for (int i = 0; i<x.size(); i++)
            instr.addAttribute(x.elementAt(i));
        instructions.add(instr);

        //2nd instr: Sum x_i * y_i -> x_i should be represented as a vector , i.e column matrix
        instr = new Instruction(MessageType.MULTIPLY);
        instr.addAttribute(y);
        for (int i = 0; i<x.size(); i++)
            instr.addAttribute(x.elementAt(i));
        instructions.add(instr);

        //3rd instr: n (num of nodes that participated in the query)
        instr = new Instruction(MessageType.PARTICIPATION);
        instructions.add(instr);

        //4th instr: for s2 computation
       /* instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(y);

        Instruction parInstr = new Instruction(MessageType.TRMULTIPLY);
        for (int i = 0; i<x.size(); i++)
            parInstr.addAttribute(x.elementAt(i));
        instr.addParam(parInstr);

        instructions.add(instr);*/


    }

    public void calculate() {
        coefficients = xxT.times(xy);
    }

    public Instruction giveInstruction() {
        if (state == instructions.size())
            return null;

        Instruction instr = instructions.get(state);
        state++;
        return instr;
    }

    public void setValue(Object value) {

        switch (state) {
            case 1:
                xxT = (Matrix) value;
                System.out.println("before inverse! ");
                xxT.print(6,2);
                xxT = xxT.inverse();
                System.out.println("inverse! ");
                xxT.print(6,2);
                break;
            case 2:
                xy = (Matrix) value;
                coefficients = xxT.times(xy);
                break;
            case 3:
                n = (Integer) value;
                //updateInstruction();
                break;
            /*case 4:
                s2 = (Double) value / (n-p);
                sigma2 = ((n-p)/n)*s2;
                break;*/
            default:
                break;
        }

    }

    private void updateInstruction() {
        // update 4th instruction
        if (state == 3) {
            Instruction instr = instructions.get(state);
            Instruction parIinstr = (Instruction) instr.getParams().get(0);
            parIinstr.addParam(coefficients);
        }

    }

    @Override
    public String toString() {
        String str = "Line: [n="+ n +" , p="+p+"]\n";


        //str = str + "β = ";
        double norm = coefficients.norm2();
        coefficients.timesEquals((double) 1/norm);
        coefficients.print(p+1,2);

        str = str +"\n"+
                "Analysis: \n s2 = "+ s2 + " , sigma2 = "+sigma2;

        return str;

    }

}
