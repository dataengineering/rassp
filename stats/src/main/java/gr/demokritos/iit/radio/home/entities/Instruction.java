package gr.demokritos.iit.radio.home.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Instruction
 *
 * <p>The <>Instruction</> indicates a part of a statistic that is suitably formatted in a summation form, in order to preserve privacy.
 *
 * @author Katerina Zamani
 */

public class Instruction {

    private MessageType type;           /* the type of computation - eg sum, square etc */

    private List<Object> attributes;    /* the attributes/columns that are involved for the computation of this instruction */

    private List<Object> params;        /* values that are need for an inner node computation - eg. +mean */

    public Instruction(MessageType type, List<Object> attributes, List<Object> params) {
        this.type = type;
        this.attributes = attributes;
        this.params = params;
    }

    public Instruction(MessageType type) {
        this.type = type;
        this.attributes = new ArrayList<>();
        this.params = new ArrayList<>();
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public List<Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Object> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(Object attr) {
        this.attributes.add(attr);
    }

    public List<Object> getParams() {
        return params;
    }

    public void setParams(List<Object> params) {
        this.params = params;
    }

    public void addParam(Object param) {
        this.params.add(param);
    }

    @Override
    public String toString() {
        String s = "Instruction "+type.toString()+": \n";

        for (int i=0; i<attributes.size(); i++) {
            s = s + attributes.get(i);
            if(params.size() !=0)
                s = s + " -/- " + params.get(i) + "\n";
        }

        return s;
    }

}
