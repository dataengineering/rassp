package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.protocols.*;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

/**
 * Created by angel on 17/2/2016.
 */
public abstract class InstructionExecutor {

    private int step = 0;

    private ComputationService computationService;

    public void setComputationService(ComputationService computationService) {

        this.computationService = computationService;
    }

    public void execute() {
        Instruction instr;
        step = 0;

        while (true) {
            instr = nextInstruction();
            step++;
            if (instr == null)
                break;
            Object result = executeInstruction(instr);
            try {
                setValue(result);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    protected abstract Instruction nextInstruction();

    protected abstract void setValue(Object value) throws Exception;

    protected int getPC() { return step; }

    protected gr.demokritos.iit.radio.home.protocols.Instruction convertInstruction(Instruction i) {
        if (i.getType() == MessageType.SUM)
            return new SumInstruction(i.getAttributes(), i.getParams());
        else if (i.getType() == MessageType.SQUARE)
            return new SquareInstruction(i.getAttributes(), i.getParams());
        else if (i.getType() == MessageType.PARTICIPATION)
            return new ParticipationInstruction(i.getAttributes(), i.getParams());
        else if (i.getType() == MessageType.THIRDPOWER)
            return new ThirdPowerInstruction(i.getAttributes(), i.getParams());
        else if (i.getType() == MessageType.FORTHPOWER)
            return new ForthPowerInstruction(i.getAttributes(), i.getParams());
        else if (i.getType() == MessageType.MULTIPLY)
            return new MultiplyInstruction(i.getAttributes(), i.getParams());
        else
            return null;
    }

    protected Object executeInstruction(Instruction instr) {
        gr.demokritos.iit.radio.home.protocols.Instruction i = convertInstruction(instr);



        try {
            Object o = (Object) Await.result(computationService.computeAsync(i), Duration.apply("100 second"));
            return o;
        } catch (Exception e) {
            e.printStackTrace();
            // FIXME
            return 0.0;
        }

        /*
        //dummy for t-test
        switch (step) {
            case 1:
                return 40.0;          //sumX
            //return 390;     //sumX
            case 2:
                return 32.0;          //sumY
            //return 385;     //sumY
            case 3:
                return 8;           //n1
            //return 5;       //n
            case 4:
                return 8;           //n2
            //return 470;     //xybar
            case 5:
                return 32.0;          //S(x-xm)^2
            //return 730;     //xxbar
            case 6:
                return 46.0;          //S(y-ym)^2
            //return 630;     //yybar
            default:
                return 0;
        }
        */

        /*
        //dummy for linear regression
        switch (step) {
            case 1:
                return 390;     //sumX
            case 2:
                return 385;     //sumY
            case 3:
                return 5;       //n
            case 4:
                return 470;     //xybar
            case 5:
                return 730;     //xxbar
            case 6:
                return 630;     //yybar
            default:
                return 0;
        }
        */

        /*
        // dummy for normal distribution
        switch (step) {
            case 1:
                return 180.0;          //sumX
            case 2:
                return 45;          //n
            case 3:
                return 80.0;           //m3
            case 4:
                return 60.0;           //m2
            case 5:
                return 180.0;          //m4
            default:
                return 0;
        }
        */

    }

}
