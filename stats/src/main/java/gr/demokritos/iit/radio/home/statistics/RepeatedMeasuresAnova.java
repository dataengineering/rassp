package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Repeated Measures ANOVA
 * <p> Implementation of r-m (within participants) anova statistic by preserving the privacy of the data source.
 * The class involves the computation of suitable parameters (eg. df,ssq,ms,f, p-value) that are necessary for analysis.</p>
 *
 * @author Katerina Zamani
 */
public class RepeatedMeasuresAnova extends InstructionExecutor  {

    private HashMap<String,Object> output = new HashMap<>(); /* the total results in a map object */
    private List groupVal = new ArrayList();   //the resulted values after the execution of instructions
    private int numGroups = 0;   //number of groups == number of the conditions
    private List<Double> intermediate = new ArrayList();

    private List<Double> df = new ArrayList(4);
    private double GM = 0.0;
    private List<Double> SSQ = new ArrayList(5);
    private List<Double> MS = new ArrayList(3);
    private List<Double> F = new ArrayList<>(2);


    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(RepeatedMeasuresAnova.class);


    public RepeatedMeasuresAnova(StatParams params) throws Exception {
        setParams(params.getInput());

        initialize(params.getQfr());
    }


    private void setParams(Map<String, Object> input) {
        // Get a set of the entries and its iterator
        Set set = input.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();

            if (entry.getKey().equals("confLevel")) {
                output.put("conf.level", entry.getValue());
            }
        }

    }

    public HashMap getResult() { return this.output; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() < 2)
            throw new Exception("Repeated Measures ANOVA (Within-Subjects) Statistic needs at least 2 sample groups!");

        numGroups = qfr.size();

        for(QfrMessage q: qfr)
            q.convert(this.getClass().getSimpleName());

        Instruction instr;

        for(int i=0; i<numGroups; i++) {
            //[0, numGroups) instr: Sum group_i
            instr = new Instruction(MessageType.SUM);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);

        }

        for(int i=0; i<numGroups; i++) {
            // [numGroups, 2*numGroups) instr: (num of nodes)
            instr = new Instruction(MessageType.PARTICIPATION);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);

        }

        for(int i=0; i<numGroups; i++) {
            // for the variance of each group....
            // [2*numGroups, 3*numGroups) instr: Sum (x_i - mean1)^2
            instr = new Instruction(MessageType.SQUARE);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);
        }

        // to compute SS_subj
        // 3*numGroups instr: Sum (xmean_i - GM)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(createQfrAvg(qfr.get(0)));
        instructions.add(instr);

    }

    /**
     * Builds a new query thas contains only a select clause.
     * The select clause is formed in a suitable way in order to fit the dummy database.
     *
     * @param q the query to be transformed
     * @return the new query
     */
    public QfrMessage createQfrAvg(QfrMessage q) {
        String select = (String) q.getSelectClause().get(0);
        String[] parts = select.split("_");

        select = "";
        for(int i=0; i<parts.length-1; i++) {
            select = select + parts[i] + "_";
        }
        select = select +"avg";

        return new QfrMessage(select);
    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        groupVal.add((double) value);


        //PARTICIPATION instructions
        if(getPC()>numGroups && getPC()<=2*numGroups) {
            if(getPC() > numGroups + 1) {
                if((int) Math.round((double) groupVal.get(getPC()-1)) != (int) Math.round((double) value))
                    throw new Exception("The computation of statistic cannot be done: Error in the number of subjects.");
            }

            updateInstruction();
        }
        //the last instruction
        else if(getPC()==3*numGroups) {
            updateInstruction();
        }


    }

    private void updateInstruction() {
        if(getPC()>numGroups && getPC()<=2*numGroups) {
            //depending on the step that we are, we can get the appropriate values regarding the specified structure of the Instructions set
            double mean = (double) groupVal.get(getPC() - 1 - numGroups) / (double) groupVal.get(getPC() - 1);

            try {
                // instruction (getPC()+numGroups)-1
                Instruction instr = instructions.get(getPC() - 1 + numGroups);
                instr.addParam(mean);
                intermediate.add(mean);
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }
        else if(getPC()==3*numGroups) {
            computeGM();
            try {
                Instruction instr = instructions.get(instructions.size()-1);
                instr.addParam(GM);
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }

        }


    }

    private void computeGM() {
        /* GM = Sum(mean_condition) / #conditions */
        for(int i=0; i<intermediate.size(); i++) {
            GM = GM + intermediate.get(i);
        }
        GM = GM/intermediate.size();
    }

    /**
     * calculates all tha appropriate variables for the summary table analysis
     */
    public void calculate() {

        df.add(0, (double) numGroups-1); //df_cond
        df.add(1, (double) groupVal.get(numGroups)-1);  // df_subj
        df.add(2, ( df.get(0)* df.get(1)) );   //df_e
        df.add(3, ( (df.get(0)+1)* (df.get(1)+1) -1) );   //df_tot ..... ( df.get(0)* df.get(1))* df.get(2)

        //////////////// SSQ //////////////////////
        double SScond = 0.0; // Sum (n_subj * (mean_cond - GM)^2)
        for(int i=0; i<numGroups; i++) {
            try {
                SScond = SScond + ( (intermediate.get(i) - GM) * (intermediate.get(i) - GM) * (double) groupVal.get(numGroups));
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }
        //SSQ.add(0, (double) Math.round((SScond * 10000.0) / 10000.0));
        SSQ.add(0, SScond);


        double SSW = 0.0;
        for(int i=2*numGroups; i<3*numGroups; i++) {
            try {
                SSW = SSW + (double) groupVal.get(i);
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }
        //SSQ.add(1, (double) Math.round((SSW * 10000.0) / 10000.0));
        SSQ.add(1, SSW);

        double SSsubj = numGroups * (double) groupVal.get(groupVal.size()-1);
        SSQ.add(2,SSsubj);

        SSQ.add(3, (SSQ.get(1) - SSQ.get(2)) ); //SSE = SSW- SSsubj

        SSQ.add(4, SScond + SSsubj + SSQ.get(3)); //SST = SScond + SSsubj + SSE

        ////////////////////// MS ///////////////////////////
        MS.add(0, SSQ.get(0)/ df.get(0));   //MScond = SScond / df_cond
        MS.add(1, SSQ.get(2)/df.get(1));   //MSsubj = SSsubj / df_subj
        MS.add(2, SSQ.get(3)/df.get(2));   //MSE = SSE / df_e

        F.add(MS.get(0) / MS.get(2));   //Fcond = MScond/MSE
        F.add(MS.get(1) / MS.get(2));   //Fsubj = MSsubj/MSE


        buildOutput();

        computePvalue();

    }

    private void computePvalue() {
        FDistribution distribution = new FDistribution( df.get(0), df.get(2)); //df_cond and df_error for the computation of F-distribution
        double pvalue;

        pvalue = 1 - distribution.cumulativeProbability(F.get(0));

        output.put("p.value", pvalue);
    }

    private void buildOutput() {
        output.put("df.cond", df.get(0));
        output.put("df.subj", df.get(1));
        output.put("df.error", df.get(2));
        output.put("df.tot", df.get(3));

        output.put("SScond", SSQ.get(0));
        output.put("SSw", SSQ.get(1));
        output.put("SSsubj", SSQ.get(2));
        output.put("SSE", SSQ.get(3));
        output.put("SST", SSQ.get(4));

        output.put("MScond", MS.get(0));
        output.put("MSsubj", MS.get(1));
        output.put("MSE", MS.get(2));

        output.put("F.cond", F.get(0));
        output.put("F.subj", F.get(1));

    }

    private String mapStr() {
        String str= "Repeated Measures Anova:\n";

        Set set = output.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + ": " + entry.getValue()+"\n";
        }

        return str;
    }

    @Override
    public String toString() {
        return mapStr();

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }
}

