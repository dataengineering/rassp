package gr.demokritos.iit.radio.home.orchestration;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.protocols.ComputationService;
import gr.demokritos.iit.radio.home.protocols.ComputationService$;
import gr.demokritos.iit.radio.home.statistics.*;

import java.io.File;
import java.util.HashMap;

/**
 * Controller
 * <p>
 * Contains the main modules(-functions) where the R-based system can communicate and call the corresponding statistic,
 * initialised by specified parameters.</p>
 *
 * @author katerina zamani
 */

public class Controller {

    static ComputationService service;

    static {
    	String conf_path = System.getProperty("config.file", "/root/application.conf");
        Config my = ConfigFactory.parseFile(new File(conf_path));
        Config config = my.withFallback(ConfigFactory.load());
        service = ComputationService$.MODULE$.apply(ActorSystem.apply("benaloh", config));
    }

    public HashMap tTestParam(StatParams params) {

        Ttest stat = null;
        try {
            stat = new Ttest(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(params.toString());

        //put benaloh protocol
        stat.setComputationService(service);

        stat.execute();

        HashMap map = stat.getResult();

        return map;
    }

    public double avgParam(StatParams params) {
        Mean stat = null;
        try {
            stat = new Mean(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public double varParam(StatParams params) {
        Variance stat = null;

        try {
            stat = new Variance(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public double stdevParam(StatParams params) {
        StDeviation stat = null;

        try {
            stat = new StDeviation(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public double corCoefParam(StatParams params) {
        CorrelationCoefficient stat = null;

        try {
            stat = new CorrelationCoefficient(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public HashMap betweenAnovaParam(StatParams params) {
        BetweenSubjectsAnova stat = null;

        try {
            stat = new BetweenSubjectsAnova(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public HashMap repeatedAnovaParam(StatParams params) {
        RepeatedMeasuresAnova stat = null;

        try {
            stat = new RepeatedMeasuresAnova(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public HashMap mixedAnovaParam(StatParams params) {
        MixedAnova stat = null;

        try {
            stat = new MixedAnova(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public HashMap chiSquareParam(StatParams params) {
        ChiSquare stat = null;

        try {
            stat = new ChiSquare(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

    public HashMap normalDistr(StatParams params) {
        NormalDistribution stat = null;

        try {
            stat = new NormalDistribution(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();

        return stat.getResult();
    }

    public HashMap lrParam(StatParams params) {
        SimpleLinearRegression stat = null;

        try {
            stat = new SimpleLinearRegression(params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        stat.setComputationService(service);

        stat.execute();


        return stat.getResult();
    }

}
