package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.log4j.Logger;

import java.util.*;

/**
 *  Mixed ANOVA
 * <p> Implementation of B-P and W-P anova statistic with one dependency variable, by preserving the privacy of the data source.
 * The class involves the computation of suitable parameters (eg. df,ssq,ms,f etc) that are necessary for analysis.
 *
 * Example:
 * ------------------------------------------------------------------------------------------------------
 *              |                                                       |
 *              |                  U: Condition                         |
 *              |-------------------------------------------------------|
 *              |    |     | c1   |  c2  | .... | cj   | Case Total     |
 *              |-------------------------------------------------------|
 *              |    |S1   |  v1  |  v2  | .... | vj   | S1_avg         |
 *              | g1 | ... | ...  |  ... | .... | ...  | ...            |
 *              |    |Sn1  | ...  |  ... | .... | ...  | Sn1_avg        |
 *              |----------------------------------------------------   |
 *              |    |     |a1*b1 |a1*b2 | .... |a1*bj | a1 (mean S_avg)|
 *              |=======================================================|
 *              |    |Sk   |  vk1 |  ... | ..... | ... | Sk_avg         |
 *              | g2 | ... |  ... |  ... | ..... | ... | ...            |
 * A: Treatment |    |Sn2  |  ... |  ... | ..... | ... | Sn2_avg        |
 *              |-------------------------------------------------------|
 *              |    |     |a2*b1 |a2*b2 | ..... |a2*bj| a2 (mean S_avg)|
 *              |=======================================================|
 *              |  .                                                    |
 *              |  .                                                    |
 *              |  .                                                    |
 *              |-------------------------------------------------------|
 *              |    |Sz   | vz1 |  ... |  ..... |  ... | Sz_avg        |
 *              | gi | ... | ... |  ... |  ..... |  ... | ...           |
 *              |    |Sni  | ... |  ... |  ..... |  ... | Sni_avg       |
 *              |------------------------------------------------------ |
 *              |    |     |ai*b1|ai*b2 |  ..... |ai*bj | ai(mean S_avg)|
 *              |=======================================================|
 *              |          | b1  |  b2  |  ..... | bj    | GM (mean bj) |
 *              |          |                     |mean cj|              |
 *---------------------------------------------------------------------------------------------------------------
 *
 * </p>
 *
 * @author Katerina Zamani
 */
public class MixedAnova extends InstructionExecutor  {

    private HashMap<String,Object> output = new HashMap<>(); /* the total results in a map object */

    private List<Double> a_i = new ArrayList<>();
    private List<Double> b_j = new ArrayList<>();
    private List<Double> a_ib_j = new ArrayList<>();
    private int count = 1; //defines the type of the instruction that was executed for the computation of ai_bj
    private List<Double> vals = new ArrayList<>();
    private List<Integer> bj_part = new ArrayList<>();
    private List<Double> ssb = new ArrayList<>();
    private List<Double> sst = new ArrayList<>();

    private List<Double> num = new ArrayList<>(); //number of subjects per group
    private int u = 0; //number of conditions eg: Time in WP
    private int a = 0; //number of groups eg: treatments in BP
    private int N = 0; //number of observations N=a*u*n , where n is the number of subjects at each group or N = u*(num_1 + num_2 + ... + num_a)

    private List<Double> df = new ArrayList();
    private double GM = 0.0;
    private List<Double> SSQ = new ArrayList();
    private List<Double> MS = new ArrayList();
    private List<Double> F = new ArrayList<>();


    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(MixedAnova.class);


    public MixedAnova(StatParams params) throws Exception {
        setParams(params.getInput());

        initialize(params.getQfr());
    }


    private void setParams(Map<String, Object> input) {
        // Get a set of the entries and its iterator
        Set set = input.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();

            if (entry.getKey().equals("confLevel")) {
                output.put("conf.level", entry.getValue());
            }
        }

    }

    public HashMap getResult() { return this.output; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 2)
            throw new Exception("Mixed ANOVA (Between & Within Subjects) Statistic needs exactly 2 sample groups!");

        initNums(qfr);

        Instruction instr;

        String select = null;
        try {
            select = (String) qfr.get(0).getSelectClause().get(0);
            if(select.equalsIgnoreCase("NULL")) {
                throw new Exception("Mixed ANOVA (Between & Within Subjects) Statistic needs one dependency variable!");
            }
        } catch (IndexOutOfBoundsException ex) {
            throw new Exception("Mixed ANOVA (Between & Within Subjects) Statistic needs one dependency variable!");
        }


        //Mean of avg_subjects for each group -> a_i
        for(int i=0; i<a; i++) {
            List<Vector<Object>> tr1 = new ArrayList<>();
            tr1.add(qfr.get(0).getTriples().get(i));
            QfrMessage q = combineQfr(select, tr1, qfr.get(1).getTriples(), 1);

            //[0, 2*a) instr: Sum and PART of avgGroup_i
            instr = new Instruction(MessageType.SUM);
            instr.addAttribute(q);
            instructions.add(instr);

            instr = new Instruction(MessageType.PARTICIPATION);
            instr.addAttribute(q);
            instructions.add(instr);

        }

        //Mean of the subjects of all groups for each condition -> b_j
        for(int i=0; i<u; i++) {
            List<Vector<Object>> tr2 = new ArrayList<>();
            tr2.add(qfr.get(1).getTriples().get(i));

            for(int j=0; j<a; j++) {
                List<Vector<Object>> tr1 = new ArrayList<>();
                tr1.add(qfr.get(0).getTriples().get(j));

                QfrMessage q = combineQfr(select, tr1, tr2, 0);

                // [2*a, 2*a+2*a*u) instr: SUM and PART of all groups and cond_j
                instr = new Instruction(MessageType.SUM);
                instr.addAttribute(q);
                instructions.add(instr);

                instr = new Instruction(MessageType.PARTICIPATION);
                instr.addAttribute(q);
                instructions.add(instr);
            }

        }


        //Mean of the subjects for each group and each condition -> a_i * b_j
        for(int i=0; i<a; i++) {
            for(int j=0; j<u; j++) {
                List<Vector<Object>> tr1 = new ArrayList<>();
                tr1.add(qfr.get(0).getTriples().get(i));

                List<Vector<Object>> tr2 = new ArrayList<>();
                tr2.add(qfr.get(1).getTriples().get(j));
                QfrMessage q = combineQfr(select,tr1, tr2, 0);

                // [2*a + 2*a*u, 2*a + 5*a*u) instr: SUM and PART of each group_i and each cond_j
                instr = new Instruction(MessageType.SUM);
                instr.addAttribute(q);
                instructions.add(instr);

                instr = new Instruction(MessageType.PARTICIPATION);
                instr.addAttribute(q);
                instructions.add(instr);

                instr = new Instruction(MessageType.SQUARE);
                instr.addAttribute(q);
                instructions.add(instr);
            }

        }

        //Square of avg_subjects for each group -> (S_i - GM)^2
        for(int i=0; i<a; i++) {
            List<Vector<Object>> tr1 = new ArrayList<>();
            tr1.add(qfr.get(0).getTriples().get(i));
            QfrMessage q = combineQfr(select, tr1, qfr.get(1).getTriples(), 1);

            //[2*a + 5*a*u, 3*a + 5*a*u) instr: SQR of avgGroup_i
            instr = new Instruction(MessageType.SQUARE);
            instr.addAttribute(q);
            instructions.add(instr);

        }


    }

    /**
     * Builds a new qfr message that contains the appropriate attributes based on the type and the conditions/treatments.
     * The select clause is formed in a suitable way in order to fit the dummy database.

     *
     * @param select the select clause variable
     * @param tr1 triple of the one of the levels from the first variable
     * @param tr2 triple of the one of the levels from the second variable
     * @param type
     * type = 0: one select clause that defines a single attribute, produced by combined triples
     * type = 1: one select clause for the avg attribute
     * type = 2: two select clauses which define two attributes respectively, produced by combined triples
     * @return the new qfr
     */
    private QfrMessage combineQfr(String select, List<Vector<Object>> tr1, List<Vector<Object>> tr2, int type) {
        if (type == 0) {
            String ss = select + "_";
            ss = ss + tr1.get(0).get(0) + "_" + tr1.get(0).get(2) + "_" + tr2.get(0).get(0) + "_" + tr2.get(0).get(2);

            return new QfrMessage(ss);
        } else if (type == 1) {
            String ss = select + "_";
            ss = ss + tr1.get(0).get(0) + "_" + tr1.get(0).get(2) + "_" + tr2.get(0).get(0) + "_avg";

            return new QfrMessage(ss);
        } else if (type == 2) {
            QfrMessage qq = new QfrMessage();
            String ss = select + "_";

            if(tr1.size() > 1) {
                for (Vector<Object> triple: tr1) {
                    String tempS = ss + triple.get(0) + "_" + triple.get(2) + "_" + tr2.get(0).get(0) + "_" + tr2.get(0).get(2);
                    qq.addSelect(tempS);
                }
            } else {
                for (Vector<Object> triple: tr2) {
                    String tempS = ss + tr1.get(0).get(0) + "_" + tr1.get(0).get(2) + triple.get(0) + "_" + triple.get(2) ;
                    qq.addSelect(tempS);
                }
            }

            return qq;
        } else
            return null;

    }

    private void initNums(List<QfrMessage> qfr) {
        //qfr.get(0) : the 'groups query'
        a = qfr.get(0).getTriples().size();

        //qfr.get(1) : the 'conditions query'
        u = qfr.get(1).getTriples().size();

    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        //for the computation of a_i
        if(getPC()<= 2*a) {
            vals.add((double) value);
            //the PARTICIPATION instructions
            if( ((getPC()-1) % 2) != 0) {

                num.add((double) value);

                updateInstruction();
            }

        }
        //for the computation of b_j
        else if(getPC()>(2*a) && getPC()<=(2*a+2*a*u)) {
            vals.add((double) value);


            //the instructions of a condition b_j have been computed
            if(getPC() % (2*a) == 0) {
                updateInstruction();
            }
        }
        //for the computation of a_i * b_j
        else if(getPC()>(2*a+2*a*u) && getPC()<=(2*a+5*a*u)) {
            vals.add((double) value);

            //square instr  ....
            if( (count % 3) == 0) {
                sst.add((double) value);
                count = 0;
            }
            else {
                if ( (count % 2) == 0) {
                    updateInstruction();
                }

            }
            count++;

        }
        else if(getPC()>(2*a + 5*a*u) && getPC()<=(3*a + 5*a*u)) {
            ssb.add((double) value);
        }



    }

    private void updateInstruction() {

        if(getPC()<= 2*a) {
            double mean = (double) vals.get(getPC()-2) / vals.get(getPC()-1);
            a_i.add(mean);
        }

        else if (getPC()>(2*a) && getPC()<=(2*a+2*a*u)) {

            double tot_sum = 0.0;
            int tot_part = 0;

            for(int i=(getPC()-2*a); i<getPC(); i++) {

                if (i%2 == 0)
                    tot_sum += vals.get(i);
                else
                    tot_part += (int) Math.round(vals.get(i));
            }
            double mean = tot_sum / tot_part;
            b_j.add(mean);
            bj_part.add(tot_part);


            if(getPC() == 2*a+2*a*u) {
                computeGM();

                //logger.info("### GM = "+GM);

                for(int k=0; k<u; k++) {
                    Instruction instr = instructions.get(2*a+5*a*u +k);
                    instr.addParam(GM);

                }

                for(int k=0; k<a*u; k++) {
                    Instruction instr = instructions.get(2*a+2*a*u+2+3*k);
                    instr.addParam(GM);
                }
            }
        }

        else if (getPC()>(2*a+2*a*u) && getPC()<=(2*a+5*a*u)) {
            double mean = (double) vals.get(getPC()-2) / vals.get(getPC()-1);
            a_ib_j.add(mean);
        }

    }

    private void computeGM() {
        /* GM = Sum(a_i) / #a_i */
        for(int i=0; i<b_j.size(); i++) {
            GM = GM + b_j.get(i);
        }
        GM = GM/b_j.size();
    }

    /**
     * calculates all tha appropriate variables for the summary table analysis
     */
    public void calculate() {

        int n = 0;
        for (int k = 0; k < num.size(); k++) {
            n = n + (int) Math.round(num.get(k));
        }
        N = n * u;

        df.add(0, (double) N - 1); //df_tot
        df.add(1, (double) (a - 1));  // df_A
        df.add(2, (double) (u - 1));   //df_U
        df.add(3, (df.get(1) * df.get(2)));   //df_UxA = df_A * df_U
        df.add(4, (double) ((N / u) - 1));   //df_BS = a*n - 1
        df.add(5, (df.get(4) - df.get(1)));  //df_S/A = df_BS - df_A
        df.add(6, (df.get(0) - df.get(4) - df.get(1) - df.get(3)));   //df_UxS/A = df_tot - df_BS - df_A - df_UxA

        //////////////// SSQ //////////////////////

        //SS_A
        double SSA = 0.0;  // SSA = Sum(n*u * (a_i - GM)^2)
        for (int i = 0; i < a_i.size(); i++) {
            SSA = SSA + (num.get(i) * u * ((a_i.get(i) - GM) * (a_i.get(i) - GM)));
        }
        SSQ.add(0, SSA);

        //SS_U
        double SSU = 0.0;   // SSU = Sum(s * (b_j - GM)^2) where s = a*n_i

        for (int i = 0; i < b_j.size(); i++) {
            SSU = SSU + (bj_part.get(i) * ((b_j.get(i) - GM) * (b_j.get(i) - GM)));
        }
        SSQ.add(1, SSU);

        //SS_Between
        double SSB = 0.0;   //SSB = Sum (u * (Savg - GM)^2) = u*num_i * Sum (Savg_i - GM)^2
        for (int i = 0; i < ssb.size(); i++) {
            SSB = SSB + u * ssb.get(i);
        }
        SSQ.add(2, SSB);

        //SS_s/A
        double SSsa = SSB - SSA; // SS_s/A -> Error1
        SSQ.add(3, SSsa);

        //SS_UxA
        double SSua = 0.0; //SS_UxA = Sum(n_i * (a_ib_j - GM)^2)
        int group = 0; //which group

        count = 1;
        for (int i = 0; i < a_ib_j.size(); i++) {
            SSua = SSua + (num.get(group) * ((a_ib_j.get(i) - GM) * (a_ib_j.get(i) - GM)));

            if ((count % a) == 0)
                group++;

            count++;

        }

        SSua = SSua - SSA - SSU;
        SSQ.add(4, SSua);

        //SS total
        double SStot = 0.0;  //SStot = Sum(S_i - GM)^2
        for (int i = 0; i < sst.size(); i++) {
            SStot = SStot + sst.get(i);
        }
        SSQ.add(5, SStot );


        //SS_UxS/A
        double SSusa = SStot - SSB - SSU - SSua;
        SSQ.add(6, SSusa);


        ////////////////////// MS ///////////////////////////
        MS.add(0, SSQ.get(0)/ df.get(1));   //MS_A = SSA / a-1
        MS.add(1, SSQ.get(3)/df.get(5));   //MS_S/A = SS_S/A / df_S/A
        MS.add(2, SSQ.get(1)/df.get(2));   //MSU = SSU / df_u
        MS.add(3, SSQ.get(4)/df.get(3));   //MS_UxA = SS_UxA / df_UxA
        MS.add(4, SSQ.get(6)/df.get(6));   //MS_UxS/A = SS_UxS/A / df_UxS/A

        F.add(MS.get(0) / MS.get(1));   //Fb = MS_A/MS_S/A
        F.add(MS.get(2) / MS.get(4));   //Fsubj1 = MSU/MS_UxS/A
        F.add(MS.get(3) / MS.get(4));   //Fsubj2 = MS_UxA/MS_UxS/A


        buildOutput();

        computePvalue();

    }

    private void computePvalue() {
        FDistribution distribution = null;

        distribution = new FDistribution( df.get(1), df.get(5));

        output.put("p.value_A", 1 - distribution.cumulativeProbability(F.get(0)));

        distribution = new FDistribution( df.get(2), df.get(6));
        output.put("p.value_U", 1 - distribution.cumulativeProbability(F.get(1)));

        distribution = new FDistribution( df.get(3), df.get(6));
        output.put("p.value_UxA", 1 - distribution.cumulativeProbability(F.get(2)));
    }

    private void buildOutput() {
        output.put("df.tot", df.get(0));
        output.put("df.A", df.get(1));
        output.put("df.U", df.get(2));
        output.put("df_UxA", df.get(3));
        output.put("df_between", df.get(4));
        output.put("df_S/A", df.get(5));
        output.put("df_UxS/A", df.get(6));

        output.put("SSA", SSQ.get(0));
        output.put("SSU", SSQ.get(1));
        output.put("SS_Between", SSQ.get(2));
        output.put("SS_S/A", SSQ.get(3));
        output.put("SS_UxA", SSQ.get(4));
        output.put("SS_tot", SSQ.get(5));
        output.put("SS_UxS/A", SSQ.get(6));

        output.put("MSA", MS.get(0));
        output.put("MS_S/A", MS.get(1));
        output.put("MSU", MS.get(2));
        output.put("MS_UxA", MS.get(3));
        output.put("MS_UxS/A", MS.get(4));

        output.put("F_A", F.get(0));
        output.put("F_U", F.get(1));
        output.put("F_UxA", F.get(2));

    }

    private String mapStr() {
        String str= "Mixed Anova:\n";

        Set set = output.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + ": " + entry.getValue()+"\n";
        }

        return str;
    }

    @Override
    public String toString() {
        return mapStr();

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }
}

