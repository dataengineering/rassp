package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Variance
 * <p>Variance for the sample. Equation: Sum (x-m)^2 / n-1</p>
 *
 * Created by Katerina Zamani
 */

public class Variance extends InstructionExecutor {

    private Double result;

    private double mean = 0.0;

    private double n = 0;

    private double sumX = 0.0;

    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(Variance.class);


    public Variance(StatParams params) throws Exception{
        initialize(params.getQfr());
    }

    public Double getResult() { return this.result; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        logger.info("....................... QFR "+qfr.get(0).toString());
        if(qfr.size() != 1)
            throw new Exception("Variance Statistic Function needs exactly one select clause!");

        Instruction instr;

        //1st instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: n (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //3rd instr: Sum (x_i - mean)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        switch (getPC()) {
            case 1:
                sumX = (Double) value;
                break;
            case 2:
                n = (Double) value;
                mean = sumX/n;
                updateInstruction();
                break;
            case 3:
                result = (Double) value/(n-1);
            default:
                break;
        }

    }

    private void updateInstruction() {
        //3rd instruction
        if (getPC() == 2) {
            Instruction instr = instructions.get(getPC());
            instr.addParam(mean);
        }

    }

    @Override
    public String toString() {
        return String.valueOf(this.result);

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
    }


}
