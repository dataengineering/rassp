package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;

import java.util.*;

/**
 * NormalDistribution
 * <p>Checks whether a sample follows a normal distribution. The test of the normality is based on the D' Agostino-Paerson omnibus method.</p>
 *
 * @author Katerina Zamani
 */
public class NormalDistribution extends InstructionExecutor {

    private double sumX = 0.0;
    private double n = 1.0;
    private double mean = 0.0;
    private double m2 = 0.0;
    private double m3 = 0.0;
    private double m4 = 0.0;

    private double pValue = 1.0;
    private String tempStr = null;
    private double signLevel = 0.95; //default significant level

    private HashMap<String,Object> output = new HashMap<>();

    private List<Instruction> instructions = new ArrayList<>();



    public NormalDistribution(StatParams params) throws Exception {
        setParams(params.getInput());

        initialize(params.getQfr());
    }

    public HashMap getResult() { return this.output; }

    private void setParams(Map<String, Object> input) {
        // Get a set of the entries and its iterator
        Set set = input.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            if (entry.getKey().equals("confLevel")) {
                signLevel = (Double) entry.getValue();
                output.put("conf.level", entry.getValue());

            }
        }
        output.put("method", "D' Agostino-Paerson omnibus");

    }

    private void initialize(List<QfrMessage> qfr) throws Exception {
        if (qfr.size() != 1)
            throw new Exception("Normal Distribution function needs one sample!");

        Instruction instr;

        //1st instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: n (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //3rd instr: Sum (x_i - xmean)^3
        instr = new Instruction(MessageType.THIRDPOWER);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);


        //4th instr: Sum (x_i - xmean)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        /* Kurtosis */

        //5th instr: Sum (x_i - xmean)^4
        instr = new Instruction(MessageType.FORTHPOWER);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);
    }

    @Override
    protected void setValue(Object value) throws Exception {

        switch (getPC()) {
            case 1:
                sumX = (Double) value;
                break;
            case 2:
                n = (Double) value;
                mean = sumX/n;
                updateInstruction();
                break;
            case 3:
                m3 = (Double) value / n;
                updateInstruction();
                break;
            case 4:
                m2 = (Double) value / n;
                updateInstruction();
                break;
            case 5:
                m4 = (Double) value / n;
                break;
            default:
                break;
        }

    }

    private void updateInstruction() {
        //3rd instruction
        if (getPC() == 2) {
            Instruction instr = instructions.get(getPC());
            //instr.addParam(values.getMeans().get(0));
            instr.addParam(mean);
        }
        //4th instruction
        else if (getPC() == 3) {
            Instruction instr = instructions.get(getPC());
            //instr.addParam(values.getMeans().get(1));
            instr.addParam(mean);
        }
        //5th instruction
        else if (getPC() == 4) {
            Instruction instr = instructions.get(getPC());
            //instr.addParam(values.getMeans().get(1));
            instr.addParam(mean);
        }
    }

    private void calculate() {
        /* Skewness */

        // computation of coefficient
        double g1 = m3 / Math.pow(m2, 3/2);
        double G1 = (Math.sqrt(n* (n-1)) / n-2) * g1;

        // inferring G1 (sample) from g1 (population)
        double SES = Math.sqrt((double) (6*n *(n-1)) / ((n-2) * (n+1) * (n+3)));
        double Zg1 = G1 / SES;

        /* Kurtosis */

        //computing coefficients
        double g2 = (m4/Math.pow(m2,2)) - 3;
        double G2 = ((double) n-1/((n-2) * (n-3)) ) * ( (double)((n+1) * g2) + 6 );


        //inferring G2 from g2
        double SEK = 2*SES * Math.sqrt((Math.pow(n,2) - 1) / ((n-3) * (n+5)));
        double Zg2 = G2/SEK;

        tempStr = "mean = "+ mean + " n = "+ n +"\n"+
                "g1 = "+g1 + " , G1 = "+G1 +"\n"+
                "g2 = "+g2 + " , G2 = "+G2+ "\n"+
                "SES = "+SES +" , SEK = "+SEK+ "\n"
                +"Zg1 = "+Zg1 + " , Zg2 = "+Zg2;

        output.put("Zg1", Zg1);
        output.put("Zg2", Zg2);

        analysis(Zg1, Zg2);
        followsNormalDistribution();
    }

    private void analysis(double Zg1, double Zg2) {
        // D' Agostino-Paerson omnibus test
        double DP = Math.pow(Zg1,2) + Math.pow(Zg2,2);

        ChiSquaredDistribution distribution = new ChiSquaredDistribution(2);
        pValue = 1 - distribution.cumulativeProbability(DP);

        output.put("p-value", pValue);
    }

    public boolean followsNormalDistribution() {
        if (pValue < signLevel) {
            output.put("follows", true);
            return true;
        }

        output.put("follows", false);
        // otherwise we can conclude if the sample follows a normal distribution, but we ll assume that it does.
        return false;
    }

    @Override
    public String toString() {
        String str = "Normal Distribution Params:\n";

        str = str + tempStr + "\n" +
                "p value = "+pValue;
        return str;
    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }


    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }
}
