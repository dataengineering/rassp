package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Correlation Coefficient
 *
 * <p>The correlation coefficient of two variables in a data sample is their covariance divided by the product of their individual standard deviations.
 * It is a normalized measurement of how the two are linearly related.
 * If the correlation coefficient is close to 1, it would indicates that the variables are positively linearly related.
 * For -1, it indicates that the variables are negatively linearly related.
 * And for zero, it would indicates a weak linear relationship between the variables.</p>
 *
 * @author Katerina Zamani
 */
public class CorrelationCoefficient extends InstructionExecutor {

    private Double result;

    private double n = 0;
    private double nTemp = 0;
    private double sumX = 0.0;
    private double sumY = 0.0;

    private double meanX = 0.0;
    private double meanY = 0.0;
    private double varX = 0.0;
    private double varY = 0.0;
    private double varXY = 0.0;

    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(CorrelationCoefficient.class);


    public CorrelationCoefficient(StatParams params) throws Exception{
        initialize(params.getQfr());
    }

    public Double getResult() { return this.result; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 2)
            throw new Exception("Correlation Coefficient Statistic Function needs exactly two variables!");

        Instruction instr;

        //1st instr: n (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: nTemp (num of nodes of the second variable): for checking
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //3rd instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //4th instr: Sum y_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //5th instr: Sum (x_i - meanX)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //6th instr: Sum (y_i - meanY)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //7th instr: Sum (x_i - meanX)*(y_i - meanY)
        instr = new Instruction(MessageType.MULTIPLY);
        instr.addAttribute(qfr.get(0));
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {
        String s = null;
        switch (getPC()) {
            case 1:
                n = (Double) value;
                break;
            case 2:
                nTemp = (Double) value;
                if((int) Math.round(n) != (int) Math.round(nTemp))
                    throw new Exception("There is inconsistency in the data of the two variables.");

                break;
            case 3:
                sumX = (Double) value;
                meanX = sumX/n;
                updateInstruction();
                break;
            case 4:
                sumY = (Double) value;
                meanY = sumY/n;
                updateInstruction();
                break;
            case 5:
                //variance of X = S (x-m)^2 / n-1
                varX = (double) value / (n-1);
                break;
            case 6:
                //variance of Y
                varY = (double) value / (n-1);
                updateInstruction();
                break;
            case 7:
                //variance of XY -> Covariance Formula
                varXY = (double) value/ (n-1);
                break;
            default:
                break;
        }

    }

    private void updateInstruction() {
        //update the 4th instruction
        if (getPC() == 3) {
            Instruction instr = instructions.get(getPC()+1);
            instr.addParam(meanX);
        }
        //update the 5th instruction
        else if (getPC() == 4) {
            Instruction instr = instructions.get(getPC()+1);
            instr.addParam(meanY);
        }
        else if (getPC() == 6) {
            Instruction instr = instructions.get(getPC());
            instr.addParam(meanX*(-1));
            instr.addParam(meanY*(-1));
        }

    }

    private void calculate() {
        result = varXY/(Math.sqrt(varX) * Math.sqrt(varY));
    }

    @Override
    public String toString() {
        return String.valueOf(this.result);

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        calculate();
    }
}
