package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;

import java.util.*;

/**
 * Mean
 * <p>The mean (average) statistic. Equation: Sum(x)/n</p>
 *
 * @author Katerina Zamani
 */
public class Mean extends InstructionExecutor {

    private Double result;

    private double n = 0;

    private double sumX = 0.0;

    private List<Instruction> instructions = new ArrayList<>();

    //final static Logger logger = Logger.getLogger(Mean.class);


    public Mean(StatParams params) throws Exception{
        initialize(params.getQfr());
    }

    public Double getResult() { return this.result; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 1)
            throw new Exception("Mean Statistic function needs exactly one select clause!");

        Instruction instr;

        //1st instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: n (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        switch (getPC()) {
            case 1:
                sumX = (Double) value;
                break;
            case 2:
                n = (Double) value;
                this.result = sumX/n;
                break;
            default:
                break;
        }

    }

    @Override
    public String toString() {
        //return this.values.toString();
        return String.valueOf(this.result);

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
    }

}