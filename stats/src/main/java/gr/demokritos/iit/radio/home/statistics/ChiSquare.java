package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Chi-Square (Pearson's method)
 *
 * <p> A chi square (X^2) statistic is used to investigate whether distributions of categorical
 * variables differ from one another.
 * In other words, Chi-square is a statistical test commonly used to compare observed data with data
 * we would expect to obtain according to a specific hypothesis.</p>
 *
 * @author Katerina Zamani
 */
public class ChiSquare extends InstructionExecutor  {

    private HashMap<String,Object> output = new HashMap<>(); /* the total results in a map object */
    private List<List<Double>> groupVal = new ArrayList<>();   //the resulted values after the execution of instructions
    private int r = 0;  //number of levels of the first categorical variable
    private int c = 0;  //number of levels of the second categorical variable

    private int limit = 1;   //counter that indicates the secondary list of groupVal that should be filled in

    private double df = 0.0;
    private List<Double> Efreq = new ArrayList<>();
    private double X2 = 0.0;

    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(ChiSquare.class);


    public ChiSquare(StatParams params) throws Exception {


        initialize(params.getQfr());
    }


    public HashMap getResult() { return this.output; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 2)
            throw new Exception("Chi-Square Statistic needs exactly 2 sample groups!");

        r = qfr.get(0).getTriples().size();
        c = qfr.get(1).getTriples().size();

        for(int i=0; i<=r; i++) {
            List<Double> l = new ArrayList(c+1);
            groupVal.add(l);
        }


        Instruction instr;
        QfrMessage qq;

        for(int i=0; i<r; i++) {
            for(int j=0; j<c; j++) {
                qq = buildQfr(qfr.get(0).getTriples().get(i), qfr.get(1).getTriples().get(j));

                instr = new Instruction(MessageType.PARTICIPATION);
                instr.addAttribute(qq);
                instructions.add(instr);
            }
        }

    }

    /**
     * Builds a new qfr message that contains the attributes of both categorical variables.
     *
     *
     * @param tr1 triple of the one of the levels from the first variable
     * @param tr2 triple of the one of the levels from the second variable
     * @return the new qfr
     */
    private QfrMessage buildQfr(Vector<Object> tr1, Vector<Object> tr2) {
        QfrMessage q = new QfrMessage();

        q.addTriple(tr1);
        q.addTriple(tr2);

        return q;
    }

    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {


        //if((double) value < 5)
          //  throw new Exception("Not enough participants to execute chi-square test.");

        groupVal.get(limit-1).add((double) value);

        //indicates that we should change the secondary list
        //put the total sum of the cells in the last cell
        if(getPC() % c == 0) {
            sumRows(groupVal.get(limit-1));
            limit++;
        }



    }

    private void sumRows(List<Double> list) {
        Double sum = 0.0;

        for(Double d: list)
            sum +=d;

        list.add(sum);
    }

    private void sumCols(int col) {
        Double sum = 0.0;

        for(int i=0; i<groupVal.size()-1; i++) {
            sum += groupVal.get(i).get(col);
        }

        groupVal.get(groupVal.size()-1).add(col, sum);
    }


    /**
     * calculate all tha appropriate variables for the summary table analysis
     */
    public void calculate() {

        df = ((r-1)*(c-1));

        for(int i=0; i<=c; i++)
            sumCols(i);

        double total = groupVal.get(groupVal.size()-1).get(c);
        double Eij = 0.0;

         for(int i=0; i<r; i++) {
             for(int j=0; j<c; j++) {
                 // Eij = ((the last element of the i-th row) * (the last element of the j-th column)) / total
                 Eij = (groupVal.get(i).get(c) * groupVal.get(r).get(j))/total;
                 Efreq.add(Eij);
             }
         }


        int count = 0;

        for (int i=0; i<r; i++) {
            for(int j=0; j<c; j++) {
               X2 = X2 +  ( (groupVal.get(i).get(j) - Efreq.get(count)) * (groupVal.get(i).get(j) - Efreq.get(count)) / Efreq.get(count) );
                count++;
            }
        }

        output.put("X2",X2);
        output.put("df", df);

        computePvalue();

    }

    private void computePvalue() {
        ChiSquaredDistribution distribution = new ChiSquaredDistribution(df);

        double pvalue = 1.0 - distribution.cumulativeProbability(X2);

        output.put("p.value", pvalue);
    }


    private String mapStr() {
        String str= "Chi-Square:\n";

        Set set = output.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + ": " + entry.getValue()+"\n";
        }

        return str;
    }

    @Override
    public String toString() {
        return mapStr();

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }
}
