package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Ttest
 * <p> Implementation of t-test statistic by preserving the privacy of the data source.
 * The class involves the computation of suitable parameters (eg. t-value, p-value) that are necessary for analysis.</p>
 *
 * @author Katerina Zamani
 */
public class Ttest extends InstructionExecutor {

    private HashMap<String,Object> output = new HashMap<>(); /* the total results in a map object */

    private double n1 = 0;
    private double n2 = 0;
    private double var1 = 0.0;
    private double var2 = 0.0;
    private double mean1 = 0.0;
    private double mean2 = 0.0;

    private double sumX,sumY = 0.0;

    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(Ttest.class);


    public Ttest(StatParams params) throws Exception {
        setParams(params.getInput());

        initialize(params.getQfr());
    }


    private void setParams(Map<String, Object> input) {
        // Get a set of the entries and its iterator
        Set set = input.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();

            if (entry.getKey().equals("alternative")) {
                output.put("alternative",(String) entry.getValue());
            }
            else if (entry.getKey().equals("mu")) {
                output.put("hypothesis",entry.getValue());
            }
            else if (entry.getKey().equals("varEq")) {
                boolean cond = Boolean.parseBoolean((String) entry.getValue());

                if(cond == true)
                    output.put("method","Normal");
                else
                    output.put("method","Welch");
            }
            else if (entry.getKey().equals("confLevel")) {
                output.put("conf.level", entry.getValue());
            }
        }

    }

    public HashMap getResult() { return this.output; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 2)
            throw new Exception("T-test needs exactly 2 groups!");

        Instruction instr;

        //1st instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: Sum y_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //3rd instr: n1 (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //4rd instr: n2 (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //5th instr: Sum (x_i - mean1)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //6th instr: Sum (y_i - mean2)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);
    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        switch (getPC()) {
            case 1:
                sumX = (double) value;
                break;
            case 2:
                sumY = (double) value;
                break;
            case 3:
                n1 = ((double) value);
                mean1 = sumX/n1;
                //put mean1 to the 5th instruction
                updateInstruction();
                break;
            case 4:
                n2 = (double) value;
                mean2 = sumY/n2;
                //put mean2 to the 6th instruction
                updateInstruction();
                break;
            case 5:
                //variance = S (x-m)^2 / n-1
                var1 = (double) value / (n1-1);
                break;
            case 6:
                var2 = (double) value / (n2-1);
                break;
            default:
                break;
        }

    }

    private void updateInstruction() {
        //update the 5th instruction
        if (getPC() == 3) {
            Instruction instr = instructions.get(getPC()+1);
            instr.addParam(mean1);
        }
        //update the 6th instruction
        else if (getPC() == 4) {
            Instruction instr = instructions.get(getPC()+1);
            instr.addParam(mean2);
        }
    }

    /**
     * calculate the t-value depending on the type method of T-test.
     */
    public void calculate() {


        ////// MAP /////////
        double denominator =  0.0;
        double tvalue = 0.0;
        double df = 0.0;

        String method = (String) output.get("method");

        if (method.equalsIgnoreCase("Welch")) {
            denominator =  (var1/n1 + var2/n2);
            tvalue = (sumX/n1 - sumY/n2 - Double.parseDouble((String) output.get("hypothesis")) ) / Math.sqrt(denominator);

            df = Math.pow(denominator,2) / ( (Math.pow(var1,2) / (Math.pow(n1,2) * (n1-1))) + (Math.pow(var2,2) / (Math.pow(n2,2) * (n2-1))) );
        }
        else {
            df = n1+n2-2;
            denominator = Math.sqrt( ((n1-1) * var1 + (n2-1) * var2) / df);
            tvalue = (sumX/n1 - sumY/n2 - Double.parseDouble((String) output.get("hypothesis")) ) / ( denominator * Math.sqrt((((double) 1/n1) + ((double) 1/n1))) );
        }
        output.put("t.value",Math.abs(tvalue));
        output.put("df", df);

        logger.info("Mean of Group1 : "+mean1);
        logger.info("Mean of Group2 : "+mean2);
        computePvalue(tvalue, df);


    }

    private void computePvalue(double tvalue, double df) {
        TDistribution distribution = new TDistribution(df);
        double pvalue;

        String alt = (String) output.get("alternative");

        if (alt.startsWith("t"))
            pvalue = 2.0 * (1-distribution.cumulativeProbability(tvalue * (-1)));
        else if (alt.startsWith("g"))
            pvalue = 1 - distribution.cumulativeProbability(tvalue);
        else
            pvalue = 1 - distribution.cumulativeProbability(tvalue * (-1));

        output.put("p.value", pvalue);
    }


    private String mapStr() {
        String str= "T-test:\n";

        Set set = output.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + ": " + entry.getValue()+"\n";
        }

        return str;
    }

    @Override
    public String toString() {
        return mapStr();

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }

}
