package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * SimpleLinearRegression
 *
 * <p>Fits a straight line : y = beta1 + beta0 * x , where y is the dependent variable and x is independent variable.
 * Linear regression with least-squares approach: a line that minimizes the sum of squared residuals of the linear regression model.
 * beta0 and beta1 are the parameters we want to calculate.</p>
 *
 * @author Katerina Zamani
 */

public class SimpleLinearRegression  extends InstructionExecutor {


    private double sumX = 0.0;      /* Σ(x_i) */
    private double sumY = 0.0;      /* Σ(y_i) */
    private double n = 0;           /* num of data points == num of nodes */
    private double nTemp = 0;
    private double xbar = 0.0;      /* sumX / n */
    private double ybar = 0.0;      /* sumY / n */
    private double xybar = 0.0;     /* Σ[ (x_i - xbar)*(y_i - ybar) ] */
    private double xxbar = 0.0;     /* Σ[ (x_i - xbar)^2 ]*/
    private double yybar = 0.0;     /* Σ[ (y_i - ybar)^2 ] */

    // for fitting
    private double beta0 = 0.0;      /* denotes the slope of the line */
    private double beta1 = 0.0;      /* denotes the  y-intercept of the line */

    // for analysis
    private double R2 = 0.0;        /* the proportion of the variance in the y variable that is predictable from the x variable */
    private double Sx = 0.0;        /* standard deviation of x */
    private double Sy = 0.0;        /* standard deviation of y */

    private List<Instruction> instructions = new ArrayList<>();

    private HashMap<String,Object> output = new HashMap<>();

    final static Logger logger = Logger.getLogger(SimpleLinearRegression.class);

    public SimpleLinearRegression(StatParams params) throws Exception {
        //setParams(params.getInput());

        initialize(params.getQfr());

    }

    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() != 2)
            throw new Exception("Linear Regression needs exactly one dependent and one independent variable!");
        Instruction instr;

        //1st instr: n (num of nodes)
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //2nd instr: nTemp (num of nodes of the independent var): just for check
        instr = new Instruction(MessageType.PARTICIPATION);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //3rdt instr: Sum x_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //4th instr: Sum y_i
        instr = new Instruction(MessageType.SUM);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);


        //5th instr: Sum (x_i - xbar) (y_i - ybar)
        instr = new Instruction(MessageType.MULTIPLY);
        instr.addAttribute(qfr.get(0));
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

        //6th instr: Sum (x_i - xbar)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(0));
        instructions.add(instr);

        //7th instr: Sum (y_i - ybar)^2
        instr = new Instruction(MessageType.SQUARE);
        instr.addAttribute(qfr.get(1));
        instructions.add(instr);

    }


    /**
     * set the calculation-value to the suitable variable depending on the state.
     *
     * @param value computed by the nodes
     */
    public void setValue(Object value) throws Exception {

        switch (getPC()) {
            case 1:
                n = (Double) value;
                break;
            case 2:
                nTemp = (Double) value;
                if((int) Math.round(n) != (int) Math.round(nTemp))
                    throw new Exception("There is inconsistency in the data of the dependent and independent variables.");
                break;
            case 3:
                sumX = (Double) value;
                xbar = sumX/n;
                break;
            case 4:
                sumY = (Double) value;
                ybar = sumY/n;
                updateInstruction();
                break;
            case 5:
                xybar = (Double) value;
                updateInstruction();
                break;
            case 6:
                xxbar = (Double) value;
                updateInstruction();
                break;
            case 7:
                yybar = (Double) value;
                break;
            default:
                break;
        }

    }

    private void updateInstruction() {
        //update 5th instruction
        if (getPC() == 4) {
            Instruction instr = instructions.get(getPC());
            instr.addParam(xbar*-1);
            instr.addParam(ybar*-1);
        }
        //6th instruction
        else if (getPC() == 5) {
            Instruction instr = instructions.get(getPC());
            instr.addParam(xbar);
        }
        //7th instruction
        else if (getPC() == 6) {
            Instruction instr = instructions.get(getPC());
            instr.addParam(ybar);
        }
    }

    /**
     * calculate beta0 and beta1 that best fit the line.
     */
    public void calculate() {

        beta0 = xybar / xxbar;
        beta1 = ybar - (beta0 * xbar);

        output.put("beta0",beta0);
        output.put("beta1",beta1);

    }

    /**
     * basic variables for regression analysis.
     */
    public void analysis() {

        Sx = Math.sqrt(xxbar / n);
        Sy = Math.sqrt(yybar / n);
        /* R2% of the variation of x independent variable can be explained by the relationship to y dependent variable. */
        R2 = Math.pow((1/n * xybar) / (Sx * Sy), 2);

        output.put("Sx",Sx);
        output.put("Sy",Sy);
        output.put("R2",R2);
    }

    public HashMap getResult() {
        logger.info(this.toString());
        return this.output;
    }

    @Override
    public String toString() {
        return "Line: y = "+beta0 +" * x + "+beta1 + "\n"+
                " xbar = "+xbar + " , ybar = "+ybar + "\n" +
                " xxbar = " + xxbar +  " , yybar = "+yybar + " xybar = " + xybar +"\n" +
                "Analysis: \n Sx = "+ Sx + " , Sy = "+ Sy + " , R^2 = "+ R2;

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
        this.analysis();
    }

}
