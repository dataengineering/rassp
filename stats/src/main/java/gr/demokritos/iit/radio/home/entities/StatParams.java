package gr.demokritos.iit.radio.home.entities;

import gr.demokritos.iit.radio.home.qfr.QfrMessage;

import java.util.*;

/**
 * StatParams
 * <p>The suitable parameters (including the queries) that are necessary for the privacy preserving statistic computation.
 *
 * @author Katerina Zamani
 */

public class StatParams {

    /* the parameters for the specific statistic*/
    private Map input = new HashMap<>();

    /*the queries that describe the data which will be involved in the statistic computation*/
    private List<QfrMessage> qfrList = new ArrayList<>();


    public StatParams(List<QfrMessage> list) {
        this.qfrList = list;

    }

    public StatParams() {}

    public Map getInput() {
        return input;
    }

    public void setInput(Map in) {
        this.input = in;
    }

    public List<QfrMessage> getQfr() {
        return qfrList;
    }

    public void addQfr(QfrMessage qfr) {
        this.qfrList.add(qfr);
    }

    @Override
    public String toString() {
        String str = "StatParams:\n - Input:\n";

        Set set = input.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + " -> "+entry.getValue() + "\n";
        }

        str = str + "------------------------------------\n" + "- Queries:\n";

        for(QfrMessage q : this.qfrList) {
            str += q.toString() + "\n";
        }

        return str;

    }

}
