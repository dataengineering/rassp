package gr.demokritos.iit.radio.home.statistics;

import gr.demokritos.iit.radio.home.entities.Instruction;
import gr.demokritos.iit.radio.home.entities.MessageType;
import gr.demokritos.iit.radio.home.entities.StatParams;
import gr.demokritos.iit.radio.home.qfr.QfrMessage;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Between Subjects ANOVA
 * <p> Implementation of one-way between anova statistic by preserving the privacy of the data source.
 * The class involves the computation of suitable parameters (eg. df,ssq,ms,f, p-value) that are necessary for analysis.</p>
 *
 * @author Katerina Zamani
 */
public class BetweenSubjectsAnova extends InstructionExecutor  {

    private HashMap<String,Object> output = new HashMap<>(); /* the total results in a map object */
    private List groupVal = new ArrayList();   //the resulted values after the execution of instructions
    private int numGroups = 0;   //number of groups

    private List<Double> df = new ArrayList(3);
    private List<Double> SSQ = new ArrayList(3);
    private List<Double> MS = new ArrayList(2);
    private double F = 0.0;


    private List<Instruction> instructions = new ArrayList<>();

    final static Logger logger = Logger.getLogger(BetweenSubjectsAnova.class);


    public BetweenSubjectsAnova(StatParams params) throws Exception {
        initialize(params.getQfr());
    }


    public HashMap getResult() { return this.output; }


    private void initialize(List<QfrMessage> qfr) throws Exception {
        if(qfr.size() < 2)
            throw new Exception("One-Way ANOVA (Between-Subjects) Statistic needs at least 2 sample groups!");

        numGroups = qfr.size();
        Instruction instr;

        for(int i=0; i<numGroups; i++) {
            //[0, numGroups) instr: Sum group_i
            instr = new Instruction(MessageType.SUM);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);

        }

        for(int i=0; i<numGroups; i++) {
            // [numGroups, 2*numGroups) instr: (num of nodes)
            instr = new Instruction(MessageType.PARTICIPATION);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);

        }

        for(int i=0; i<numGroups; i++) {
            // for the variance of each group....
            // [2*numGroups, 3*numGroups) instr: Sum (x_i - mean1)^2
            instr = new Instruction(MessageType.SQUARE);
            instr.addAttribute(qfr.get(i));
            instructions.add(instr);
        }

    }


    /**
     * set the computed value to the suitable variable depending on the running state.
     *
     * @param value computed by the nodes
     */
    @Override
    protected void setValue(Object value) throws Exception {

        groupVal.add((double) value);

        if(getPC()>numGroups && getPC()<=2*numGroups) {
            updateInstruction();
        }


    }

    private void updateInstruction() {
        //depending on the step that we are, we can get the appropriate values regarding the specified structure of the Instructions set
        double mean = (double) groupVal.get(getPC()-1-numGroups)/ (double) groupVal.get(getPC()-1) ;

        try {
            // instruction (getPC()+numGroups)-1
            Instruction instr = instructions.get(getPC() - 1 + numGroups);
            instr.addParam(mean);

        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }


    }

    /**
     * calculate all tha appropriate variables for the summary table analysis
     */
    public void calculate() {

        df.add(0, (double) numGroups-1); //df_g

        int N = 0;
        for(int i=numGroups; i<2*numGroups; i++) {
            try {
                N = N + (int) Math.round((double) groupVal.get(i));
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }
        df.add(1, (double) N-numGroups);  // df_e
        df.add(2, (double) N -1);   //df_tot




        double GM = 0.0;
        for(int i=0; i<numGroups; i++) {
            GM = GM + (double) groupVal.get(i);
        }
        GM = GM/N;


        double SSB = 0.0; // Sum (n_i * (mean_i - GM)^2)
        for(int i=0; i<numGroups; i++) {
            try {
                double mean =  (((double) groupVal.get(i) / ((double) groupVal.get(i + numGroups))));

                SSB = SSB + ( (mean - GM) * (mean - GM) * (double) groupVal.get(i + numGroups));
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }

        SSQ.add(0, SSB);

        double SSE = 0.0;
        for(int i=2*numGroups; i<3*numGroups; i++) {
            try {
                SSE = SSE + (double) groupVal.get(i);
            } catch (IndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }
        }
        SSQ.add(1, SSE);
        SSQ.add(2, SSE+SSB);


        MS.add(0,  SSQ.get(0)/df.get(0));   //MSB = SSB / df_g
        MS.add(1,  SSQ.get(1)/ df.get(1));   //MSE = SSE / df_e

        F = MS.get(0) / MS.get(1);   //F = MSB/MSE


        buildOutput();

        computePvalue();

    }

    private void computePvalue() {
        FDistribution distribution = new FDistribution(df.get(0), df.get(1));
        double pvalue;

        pvalue = 1 - distribution.cumulativeProbability(F);

        output.put("p.value", pvalue);
    }

    private void buildOutput() {
        output.put("df.g", df.get(0));
        output.put("df.e", df.get(1));
        output.put("df.tot", df.get(2));

        output.put("SSB", SSQ.get(0));
        output.put("SSE", SSQ.get(1));
        output.put("SST", SSQ.get(2));

        output.put("MSB", MS.get(0));
        output.put("MSE", MS.get(1));

        output.put("F.value", F);

    }

    private String mapStr() {
        String str= "One-Way Anova:\n";

        Set set = output.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            str = str + entry.getKey() + ": " + entry.getValue()+"\n";
        }

        return str;
    }

    @Override
    public String toString() {
        return mapStr();

    }

    @Override
    protected Instruction nextInstruction() {
        if (getPC() >= instructions.size())
            return null;
        else
            return instructions.get(getPC());
    }

    @Override
    public void execute() {
        super.execute();
        this.calculate();
    }
}
